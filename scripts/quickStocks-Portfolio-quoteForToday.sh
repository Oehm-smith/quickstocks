#!/usr/bin/env bash
#
# Run quickstocks with ordered list of portfolio symbols
if [[ -e .env ]]; then
  source .env
else
  echo Scripts require a .env file to get portfolio holdings
  exit 1
fi
echo curl "http://localhost:9005/quote/price?symbols=${indices},${symbols}&format=csvPortfolio"

curl "http://localhost:9005/quote/price?symbols=${indices},${symbols}&format=csvPortfolio"
