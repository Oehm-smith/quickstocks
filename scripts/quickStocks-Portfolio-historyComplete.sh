#!/usr/bin/env bash
#
# Run quickstocks with ordered list of portfolio symbols
if [[ -e .env ]]; then
  source .env
else
  echo Scripts require a .env file to get portfolio holdings
  exit 1
fi
>&2 echo curl "http://localhost:3000/history?symbols=${indices},${symbols}&format=csvPortfolio"

curl "http://localhost:3000/history?symbols=${indices},${symbols}&format=csvPortfolio"
