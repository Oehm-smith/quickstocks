#!/usr/bin/env bash
#
# Run quickstocks with ordered list of portfolio symbols using todays date
# It will only return anything after the markets close
#
# 15.7.2023 - this has definitely become necessary as /quote no longer works due to a change in yahoo-finance API

if [[ -e .env ]]; then
  source .env
else
  echo Scripts require a .env file to get portfolio holdings
  exit 1
fi

#DATE_FROM="2023-07-15"
#DATE_TO="2023-07-16"
# FROM is today, TO is +1 day
DATE_FROM=$(date +%Y-%m-%d)
DATE_TO_SECS=$(( $(date "+%s") + 1 * 60 * 60 * 24 ))
DATE_TO=$(date -r $DATE_TO_SECS +%Y-%m-%d)

echo curl "http://localhost:9005/history?from=${DATE_FROM}&to=${DATE_TO}&symbols=${indices},${symbols}&format=csvPortfolio"

curl http://localhost:9005/history\?from\=${DATE_FROM}\&to\=${DATE_TO}\&symbols\=${indices},${symbols}\&format\=csvPortfolio
