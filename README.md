# Quick Stocks

## Running
    git co git@gitlab.com:Oehm-smith/quickstocks.git
    cd quickstocks
    npm ci
    npm start
    
The RESTful endpoints have been exposed at http://localhost:3000 and they are listed in the console after start-up.

## Setting up portfolio

I have used a spreadsheet for years to manage the history of my portfolio.  It has symbols listed across the top and I have been manually updating it weekly by adding a new row to the sheet.  A row looks like this:

| Date | ^axjo | cba.ax | bhp.ax | ... |
|------|-------|----------|-----------|----|
| 2020-05-29 | 5755.700195 | 63.75 | 34.639999 | ...

Where i add indexes at the front before each symbol.

The index and symbols are setup though a `.env` file in the root of the project:

    indices=^axjo,^axko,...
    symbols=cba.ax,bhp.ax,...

Use https://finance.yahoo.com/quote/ to determine the symbols.  Remember that most exchanges require a suffix eg. `.ax` for the Australian Exchange shares.

This seems cumbersome and not very user friendly.  Fear not!  The roadmap for this or a related project has a client UI to setup portfolios.  Persistence over time will follow.

# Getting data in to your spreadsheet

Your shell needs to have the `curl` tool installed.  Google for how to do that if it is not already installed.

After starting the server as per the above:
* Run the `./scripts/quickStocks-Portfolio-quoteForToday.sh` script
* Copy just the values
* Open Excel
  * Put cursor in cell of the next available row of a DUMMY SPREADSHEET
  * Home Ribbon > Paste (clipboard icon) > Use Text Import Wizard
  * Change to `comma delimitered` (untick 'spaces')
  * Change date field to `Date: YMD`
  * Hit `Finish` and the CSV entry is import
  * Copy the row
  * Go to your portfolio spreadsheet and click in the next available row
  * Home Ribbon > Paste (clipboard icon) > Paste Special
  * Choose 'values' and 'add'
* OpenOffice / LibreOffice doesn't have a text import wizard.  Instead you need to save as a `.csv` file, open that and copy in to your main spreadsheet:
  * After the script runs it writes to the terminal what it is running.  Something like:
       
       `curl http://localhost:3000/quote/price?symbols=^axjo,cba.ax,bhp.ax&format=csvPortfolio`

* ALTERNATIVELY Copy the URL the script spits out eg. `http://localhost:3000/quote/price?symbols=^axjo,cba.ax,bhp.ax&format=csvPortfolio` and paste into your browser.  This should save or open a `.csv` file.  Copy and paste special into your Portfolio spreadsheet using 'value' and 'add'
  
## Historical values

When you run the `./scripts/quickStocks-Portfolio-allHistory.sh` script it will retrieve all historical values that are available for the indices and symbols listed in the `.env` file.  At some point in time, only end-of-week or end-of-month values have been retained by the data provider.  After that point daily values are available.  The only way to get more fine-grained values (hourly for example) is to run the `./scripts/quickStocks-Portfolio-quoteForToday.sh` script since it returns the latest values.

The roadmap for QuickStocks includes persisting the quote data to database and running the lookup regularly, such as hourly.
  
## History, Notes and Motivation

The motivation behind this application (it is actually a system of RESTful services and some bash scripts) is that I could not find what I was looking for, which was:
* OpenSource and free
* Easily extensible (think of forking in GitHub)
* Cross-platform Client-Server based with exposed RESTful services
* Customisable clients
* The data provided by some financial services API.  It is best if it is free although I am happy to include ability for users to supply their own account for some provider Quick Stocks supports.

The main application I've been using for years now is JStock - https://jstock.org/.  It is OpenSource and free, hosted on Github, runs in Java and thus is cross-platform and gets its data from the free Yahoo Finance service.  However it is one ugly piece of code! (Sorry [yccheok](https://github.com/yccheok) - it has served me well over the years but I've been unable to try and modify the code in any way).  The client is integrated (and interwoven it seems) in to the same code as what I call the "backend services".  The business logic is integrated through both the client and server.  This means it is not easily extensible (I know as I tried).

However, QuickStocks (or some subsequent and related project) is new and does not contain a lot of the same functionality - such as portfolios and persistence.

# Data providers

Quick Stocks uses Yahoo Finance.  I thought their API had been shutdown, however after some investigation I found them to still exist.  They are free with reasonable usage terms and conditions.

I started using AlphaVantage as the shares data API service.  It worked well and was extensive.  However, suddenly in early May 2020 it no longer seems to support the Australian Stock Exchange (ASX) - returning `[]` data.  There are not many choices for the ASX data.  Finnhub is another that used to supply ASX data but then suddenly didn't.  I followed up with the ASX and they said that both these were breaking the terms and conditions.  The ASX recommended Bullcharts and Iguana2.

I sent emails to both these.  I received no replies from Iguana2.  I had an email conversation with a representative from Weblink.  He said that the API is described at https://shawcloud.weblink.com.au/api.asmx.  I played around a bit and received `[]` responses (even for US stocks).  This person told me that i am unauthorised.  I have sent a further request to learn how i can get authorised.  I was basically told that if I have to ask I can't afford it.

Before finding out that access to ASX data is limited (given it didn't used to be), I had architected my application to be able to pull data from multiple providers.  The intention was to use the free tier of providers since this tier only allow a certain number of API calls or impose some other restriction.  Theoretically with enough providers you could get all the data you need. Or you could retrieve historical data at hourly periods.  Or real time data.  The mind boggles; especially around the growing complexity :)
   
# Tools

I have provided scripts as introduced in [#GettingDataInToYourSpreadsheet](Getting data in to your spreadsheet) that can be used when running locally.

# Deploying to Heroku

Running locally is ok, but you may want access when you are not connected to localhost.  Lets discuss deployment to a cloud provider  I choose Heroku due to its apparent simplicity.

I wrote this question and answer in [https://stackoverflow.com/questions/59587296/how-to-deploy-a-typescript-nodejs-and-express-app-to-heroku](https://stackoverflow.com/questions/59587296/how-to-deploy-a-typescript-nodejs-and-express-app-to-heroku) and used it to enable easy heroku deployment.  Theoretically the changes should also enable deployment to Azure, AWS, Digital Ocean etc...

The steps:

     heroku login
     heroku create quickstocks-2 # I already have 'quickstocks'!  
     # You can use it however I only use the free tier so can take up to 30 seconds to startup
     heroku buildpacks:add zidizei/typescript
     heroku buildpacks:add heroku/nodejs
     git push heroku master
 
 You can monitor the install and watch it running with:
 
     heroku logs --tail
     
If you have a portfolio you always want to check the prices for, set them up as described in [#SettingUpPortfolio](Setting up portfolio), run the web app locally and run the `./scripts/quickStocks-Portfolio-quoteForToday.sh` script.  It will print out the URL in the terminal (actually it will do this regardless of if the app is running locally or not).  Just copy that replacing `localhost:3000` with your heroku domain (eg. `https://quickstocks-2.herokuapp.com`), run it in your browser and add it as a bookmark in your browser.  After running in the browser it should prompt you to save a `.csv` file.

My instance can be seen running with this example URL:

[https://quickstocks.herokuapp.com/quote/price?symbols=^axjo,cba.ax,bhp.ax&format=csvPortfolio](https://quickstocks.herokuapp.com/quote/price?symbols=^axjo,cba.ax,bhp.ax&format=csvPortfolio)

# Future road map

I will eventually setup the github project https://gitlab.com/Oehm-smith/quickstocks to have a road map.

Currently as of release 0.2.0, QuickStocks is not much different to using Yahoo Finance directly, excepting for the Output Format arguments (https://quickstocks.herokuapp.com/api/) which I needed to enable the quick updating of my spreadsheet-bound-portfolio.

A highlight of the road map is portfolio management.  The most granular level of historical data from Yahoo Finance is daily.  However the quote lookup returns the latest value (I'm not sure of the update frequency though it would appear to be multiple times an hour).  The plan is to pull quotes multiple times an hour.  Over time the amount of data would be whittled down to daily - perhaps keep 20min interval of data for a week (but like most things, this decision would be configurable).

Another highlight is a web client.  The intention is to keep this completely separate to enable anyone to write their own.
 
# Footer

Quick Stocks is created using the <a href="http://nestjs.com/" target="blank">NestJS</a> framework.  See [README-nestjs.md](README.md)
