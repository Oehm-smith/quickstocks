 # Architecture
 
 ## Services
 
    services / serviceProviders / serviceProvider.API.ts (this)
    services / serviceProviders / serviceProvider.ts (Implementation of serviceProvider.API) - internally this is what gets called
    services / serviceProviders / <Providers> / <provider>ServiceProvider.ts - <Providers> impl of serviceProvider.API and a bridge between serviceProvider.ts and the <provider>s API Impl
    services / serviceProviders / <Providers> / <provider>.API - the API for <Provider>
    services / serviceProviders / <Providers> / <provider>.ts - the <Provider>'s implementation of <provider>.API and calls the cloud service

       The serviceProvider.ts Impl will use a factory to determine which <Provider> to use

## Models

    models / serviceProviderModel - internally the data structure that is used and persisted
    models / <provider>Model - the format used for the data returned from external cloud provider calls.  EXCEPT it may be a normalised form requiring some translation.
        For example AlphaVantage returns things like "TimeSeries (Daily)" and "1. Open".  But these are problems as JS Object keys so get normalised to "TimeSeriesDaily" and "Open"
        
## RESTful endpoints

See https://quickstocks.herokuapp.com/api
