const { pathsToModuleNameMapper } = require('ts-jest');
const { compilerOptions } = require('./tsconfig');

module.exports = {
  moduleFileExtensions: [
    'js',
    'json',
    'ts',
  ],
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: [
    '**/*.(t|j)s',
  ],
  coverageDirectory: '../coverage',
  testEnvironment: 'node',
  preset: 'ts-jest',
  moduleNameMapper: {
    '^@dataFiles/(.*)$': '<rootDir>/test/testData/$1'
  },
  verbose: true,
  bail: false,
  silent: false,
  setupFiles: ['<rootDir>/test/setup.ts'],
};