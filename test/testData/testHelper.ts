import fs from 'fs';
import moment from 'moment';
import {
  ServiceHistorical,
  ServiceHistoricalBatch,
} from '../../src/services/serviceProviders/ServiceProviderHistory.model';
import { DebugFormatter } from '../../src/services/formatters/DebugFormatter';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import {
  ChartResult,
  MapChartResultsArray, ObjectChartResultsArray,
} from '../../src/services/serviceProviders/YahooFinance2/YahooHistory2.model';

export class TestHelper {
  // Historical

  /**
   *
   * @param file - must be format of ChartResultArray | MapChartResultsArray - with 'meta' and 'quote' sections and wrapped in symbol Map for MapChartResultsArray
   * @param startDateString
   * @param endDateString
   * @return Promise<MapChartResultsArray> even if only one symbol (ie. input file is ChartResultArray)
   *
   * This will return an object and the date strings will be turned in to Dates.  The data will only include quotes with dates between (and including)  startDateString and endDateString
   */
  static getAndPrepareTestDataServiceHistorical = (
    file,
    startDateString, endDateString,
  ): Promise<ServiceHistorical | ServiceHistoricalBatch> => {
    throw Error('Not defined')
  }

    static getAndPrepareTestData = (
    file,
    startDateString, endDateString,
  ): Promise<ChartResultArray | MapChartResultsArray> => {
    const data = fs.readFileSync(file, 'utf8');

    let yahooFinanceModel = JSON.parse(data); // as Historical;
    const WRAP_IT = true
    if (yahooFinanceModel.meta) {
      // Only a single symbol
      return TestHelper.prepareChartResultsArray(yahooFinanceModel as ChartResultArray, startDateString, endDateString, WRAP_IT) as Promise<MapChartResultsArray>
    } else {
      // Get the first entry in the object, which should be a symbol like 'cba.ax'.  And that should contain 'meta'.  If so then data contains chart data for 1+ symbols
      const symbols = Object.keys(yahooFinanceModel);
      if (symbols.length) {
        if (yahooFinanceModel[symbols[0]].meta) {
          return TestHelper.prepareMapChartResultsArray(yahooFinanceModel as ObjectChartResultsArray, startDateString, endDateString)
        } else {
          throw new Error(`TestHelper > getAndPrepareTestData - YahooFinance data has symbols: ${JSON.stringify(symbols)}, but there is no 'meta' under first one: ${JSON.stringify(yahooFinanceModel[symbols[0]])} so must be something wrong with test data`)
        }
      } else {
        throw new Error(`TestHelper > getAndPrepareTestData - YahooFinance data has no symbols so must be something wrong with test data`)
      }
    }
  }

  /**
   * Make date strings in to Dates and extract data for given date range
   * @param yahooFinanceModel w data for one symbol
   * @param startDateString return data after (and including) this data
   * @param endDateString return data before (and including) this data
   * @param wrapIt (optional and default is false).  If true then return this as a MapChartResultsArray for the symbol
   * @private
   */
  private static prepareChartResultsArray(yahooFinanceModel: ChartResultArray, startDateString: string, endDateString: string, wrapIt=false):Promise<ChartResultArray|MapChartResultsArray> {
    const fixDates = (chart: ChartResult[]): ChartResult[] => {
      for (const data of chart) {
        const theDate = new Date(data.date);
        data.date = theDate;
      }
      return chart;
    };

    const betweenDates = (fixedDatesChart: ChartResult[]) => {
      if (!startDateString || !endDateString) {
        return fixedDatesChart;
      }

      const chartResultsBetweenDates: ChartResult[] = [];
      const mStartDate = moment(startDateString);
      const mEndDate = moment(endDateString);
      for (const chartData of fixedDatesChart) {
        const dpDate = moment(chartData.date)
        if (dpDate.isBetween(mStartDate, mEndDate, 'day', '[]')) {
          chartResultsBetweenDates.push(chartData);
        }
      }

      return chartResultsBetweenDates;
    }

    const fixedDatesChart = fixDates(yahooFinanceModel["quotes"])

    const chartWDatesBounded = betweenDates(fixedDatesChart);
    const preparedChartResults = {
      meta: yahooFinanceModel['meta'],
      quotes: chartWDatesBounded
    }

    if (wrapIt) {
      const mapChartResultsArray = new Map<string, ChartResultArray>();
      mapChartResultsArray.set(preparedChartResults['meta'].symbol.toUpperCase(), preparedChartResults as ChartResultArray);
      return Promise.resolve(mapChartResultsArray);
    }
    return Promise.resolve(preparedChartResults as ChartResultArray);
  }

  /**
   * Make date strings in to Dates and extract data for given date range - for all symbols in the map
   * @param yahooFinanceModel
   * @private
   */
  private static async prepareMapChartResultsArray(yahooFinanceModel: ObjectChartResultsArray, startDateString, endDateString):Promise<MapChartResultsArray> {
    const symbols = Object.keys(yahooFinanceModel)
    const mapChartResultsArray = new Map<string, ChartResultArray>();
    for (const s of symbols) {
      const symbolChartArray = yahooFinanceModel[s]
      const preparedData = await TestHelper.prepareChartResultsArray(symbolChartArray as ChartResultArray, startDateString, endDateString)
      mapChartResultsArray.set(s.toUpperCase(), preparedData as ChartResultArray);
    }
    return Promise.resolve(mapChartResultsArray);
  }

  // const wantedDates = (
  //     historical: ServiceHistorical,
  //     startDate: string,
  //     endDate: string,
  //   ): ServiceHistorical => {
  //     if (!startDate || !endDate) {
  //       return historical;
  //     }
  //     const newHistorical: ServiceHistorical = [];
  //     const mStartDate = moment(startDate);
  //     const mEndDate = moment(endDate);
  //     for (const dataPoint of historical) {
  //       const mDPDate = moment(dataPoint.date);
  //       if (mDPDate.isBetween(mStartDate, mEndDate, 'day', '[]')) {
  //         newHistorical.push(dataPoint);
  //       }
  //     }
  //
  //     return newHistorical;
  //   };

    // const fixDates = (financeData: ServiceHistorical): ServiceHistorical => {
    //   for (const data of financeData) {
    //     const theDate = new Date(data.date);
    //     data.date = theDate;
    //   }
    //   return financeData;
    // };


    // Need to find out if Batch data (has a Map) or just one symbol (is Array) and massage data accordingly
    // if (Array.isArray(yahooFinanceModel)) {
    //   yahooFinanceModel = wantedDates(
    //     fixDates(yahooFinanceModel),
    //     startDate,
    //     endDate,
    //   );
    //   // yahooFinanceModel = wantedDates(yahooFinanceModel, startDate, endDate)
    // } else {
    //   const yahooFinanceModelMap = new Map<string, ServiceHistorical>();
    //   Object.keys(yahooFinanceModel).forEach((symbol) => {
    //     yahooFinanceModelMap.set(
    //       symbol,
    //       wantedDates(fixDates(yahooFinanceModel[symbol]), startDate, endDate),
    //     );
    //   });
    //   yahooFinanceModel = yahooFinanceModelMap;
    // }


    // Convert to Dates as the YahooFinance npm lib does
   /* const debug = new DebugFormatter();
    console.log(
      `yahooFinanceModel in stub: ${debug.formatHistorical(yahooFinanceModel)
    );

    // Massage the test data returned to be just the requested dates
    return Promise.resolve(yahooFinanceModel);*/
  // }

  // Quotes
  // The regularMarketTime field will be the time the market closes (I believe as can't find any details)
  // If this is running just before the market opens on a Tue after Fri and Mon public holidays then the
  // close will be last Thursday or 4.5 days back.
  static checkMarketCloseWithin5Days (marketDate) {
    const today = moment();
    const fiveDaysAgo = today.clone();
    fiveDaysAgo.subtract(5, 'd');
    const marketClosed = moment(marketDate);
    expect(marketClosed.isBetween(fiveDaysAgo, today)).toBeTruthy();
  }

  static checkPrice(price, symbol) {
    expect(price).toBeDefined();
    expect(price.exchange.toLocaleLowerCase()).toEqual('asx');
    expect(price.symbol.toLocaleLowerCase()).toEqual(symbol);
    TestHelper.checkMarketCloseWithin5Days(price.regularMarketTime);
  }
}
