// This from ChatGPT chat

import { afterEach, beforeAll, beforeEach, describe, expect, it, jest, test } from '@jest/globals';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

function processStrings(
  strings: string[],
  asyncFunction: (input: string) => Promise<string>,
): Promise<string>[] {
  const promises = strings.map((str) => asyncFunction(str)); // Return promises for each string
  console.log('Generated promises:', promises); // Log promises
  return promises;
}

const strings = ['01', '02', '03', '04'
  , '11', '12', '13', '14'
  , '31', '32', '33', '34'
  , '21', '22', '23', '24'
  , '51', '52', '53', '54'];

function exampleAsyncFunction(input: string): Promise<string> {
  const to = Math.random() * 100000;
  console.log(`create promise for: ${input}`);
  return new Promise((resolve) => {
    // resolve(`Result for ${input}`);
    setTimeout(() => {
      // console.log(`resolving`)
      resolve(`Result for ${input} w to: ${to}`);
    }, to);
  });
}

test('real timers example', async () => {
  const result = await new Promise((resolve) => {
    setTimeout(() => resolve('Resolved!'), 1000);
  });
  console.log(`result: `, result);
  expect(result).toBe('Resolved!');
});

describe('test handling multiple promises', () => {
  beforeAll(() => {
    jest.useFakeTimers();
  });
  beforeEach(() => {
  });
  afterEach(() => {
    // jest.advanceTimersByTime(20000);
    jest.runAllTimers(); // or jest.advanceTimersByTime(1000)
  });

  it('test timeout', async () => {
    setTimeout(() => console.log('Timeout executed'), 1000);
  });
  it('handle each as it resolves', (done) => {
    console.log(`--> running handle each as it resolves`);
    const promises = processStrings(strings, exampleAsyncFunction);
    // console.log("Promises created:", promises);
    promises.forEach((promise, index) => {
      // console.log(`  try promise at index:`, index)
      promise.then((result) => {
        console.log(`String ${strings[index]} resolved to:`, result);
      })
        .catch(err => console.error(`Error for ${strings[index]}`, err));
    });
    done();
  });
  it('Wait for All Promises to Resolve', (done) => {
    try {
      console.log(`--> running Wait for All Promises to Resolve`);
      const promises = processStrings(strings, exampleAsyncFunction);
      // console.log("Promises created:", promises);
      Promise.all(promises).then((results) => {
          console.log('All promises resolved:', results);
        })
        .catch(err => console.error(`Error resolving promises`, err))
        .finally(() => {
          console.log(`after Promise.all`);
        });
      done();
    } catch (error) {
      console.error('unexpected error: ', error);
    }
  });
});
