// https://stackoverflow.com/questions/51105622/how-to-ensure-jest-fails-on-unhandledrejection

process.on('unhandledRejection', (reason) => {
  console.log("unhandledRejection - ", reason); // log the reason including the stack trace
  throw reason;
});

process.on('uncaughtException', (err) => {
  console.error('Uncaught Exception:', err);
});