import * as sinon from 'sinon';
import { Test, TestingModule } from '@nestjs/testing';
import { ServiceProvider } from '../services/serviceProviders/serviceProvider';
import { QuoteController } from './quote.controller';
import { OutFormat } from './QueryTypes';
import { MockResponse } from '../utils/TestUtils';
import { afterEach, beforeEach, describe, expect, it, xdescribe } from '@jest/globals';
import { YahooFinance2ServiceProvider } from '../services/serviceProviders/YahooFinance2/YahooFinance2.serviceProvider';
import { YahooFinance2 } from '../services/serviceProviders/YahooFinance2/YahooFinance2';

xdescribe(`quote Controller`, () => {
  let quoteController;
  let response;
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [QuoteController],
      providers: [ServiceProvider, YahooFinance2ServiceProvider, YahooFinance2],
    }).compile();

    quoteController = app.get<QuoteController>(QuoteController);
    response = new MockResponse();
  });

  describe(`quotePrice in csvPortfolio format`, () => {
    it(`cba quote csvPortfolio`, async () => {
      await quoteController.getQuotePrice(
        'cba.ax',
        OutFormat.csvPortfolio,
        response,
      );
      const quote = response.body;
      console.log(`quotePrice: ${JSON.stringify(quote, null, 2)}`);
      expect(quote).toBeDefined();
    });

    it(`^axjo, cba, bhp quote`, async () => {
      await quoteController.getQuotePrice(
        '^axjo,cba.ax, bhp.ax',
        OutFormat.csvPortfolio,
        response,
      );
      const quote = response.body;
      console.log(`quotePrice: ${JSON.stringify(quote, null, 2)}`);
      expect(quote).toBeDefined();
    });
  });

  describe(`quotePrice in json format`, () => {
    it(`cba quote json format`, async () => {
      await quoteController.getQuotePrice('cba.ax', OutFormat.json, response);
      const quote = response.body;
      console.log(`quotePrice: ${JSON.stringify(quote, null, 2)}`);
      expect(quote).toBeDefined();
      expect(quote['cba.ax']).toBeDefined();
      expect(quote['cba.ax'].price).toBeDefined();
      expect(quote['cba.ax'].price.maxAge).toBeDefined();
    });

    it(`^axjo, cba, bhp quote in json format`, async () => {
      await quoteController.getQuotePrice(
        '^axjo,cba.ax, bhp.ax',
        OutFormat.json,
        response,
      );
      const quote = response.body;
      console.log(`quotePrice: ${JSON.stringify(quote, null, 2)}`);
      expect(quote).toBeDefined();
    });
  });

  describe(`HTTP Response showing failure when the symbol doesnt exist`, () => {
    const serviceProvider = new ServiceProvider(null);
    let serviceProviderStub;
    let quoteController: QuoteController;
    const badSymbol = 'zzz.zz';
    beforeEach(async () => {
      serviceProviderStub = sinon
        .stub(serviceProvider, 'quote')
        .throws(Error(`Quote not found for ticker symbol: ${badSymbol}`));
      quoteController = new QuoteController(serviceProvider);
    });

    afterEach(async () => {
      serviceProviderStub.restore();
    });
    it(`run test`, async () => {
      // symbol doesn't exist - HTTP Response showing failure
      await quoteController.getQuotePrice(badSymbol, OutFormat.json, response);
      expect(response.httpStatus).toEqual(400);
      expect(response.body).toBeDefined();
      expect(response.body.response.error).toContain(badSymbol);
    });
  });
});
