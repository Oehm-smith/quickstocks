import { Controller, Get, HttpException, HttpStatus, Query, Res } from '@nestjs/common';
import { ServiceProvider } from '../services/serviceProviders/serviceProvider';
import { FormatterFactory } from '../services/formatters/FormatterFactory';
import { QuoteModules } from '../services/serviceProviders/ServiceProviderQuote.model';
import { ApiQuery } from '@nestjs/swagger';
import { OutFormat } from './QueryTypes';
import { Period } from '../services/serviceProviders/serviceProvider.API';

@Controller('quote')
export class QuoteController {
  constructor(private readonly serviceProvider: ServiceProvider) {
  }

  /**
   * Perform symbol(s) quote lookup.
   * @param params
   *  - symbols (comma delimited)
   *  - format: json (default), csv, csvPortfolio (allow to copy and paste in to my current spreadsheet as a solution until I get portfolio functionality working)
   */
  @Get('price')
  @ApiQuery({ name: 'format', enum: OutFormat })
  @ApiQuery({ name: 'symbols' })
  async getQuotePrice(
    @Query('symbols') symbols: string,
    @Query('format') format: OutFormat = OutFormat.json,
    @Res() response,
  ): Promise<any> {
    const symbolsArray = symbols ? symbols.split(/,\W*/) : [];
    console.log(
      `getQuotePrice - symbols: ${symbolsArray}, format: ${format}`,
    );

    try {
      const result = await this.serviceProvider.quote(symbolsArray, [
        QuoteModules.price,
      ]);
      // const debugFormatter = new DebugFormatter();
      // console.log(colours.bgCyan.black(`getQuote - symbols: ${JSON.stringify(symbols)} ...\n`
      //   + `${debugFormatter.formatQuote(result)}`));
      const formatter = FormatterFactory.formatter(format, Period.d);
      const formattedOutput = formatter.formatQuote(result);
      // console.log(colours.black.bgCyan(`${JSON.stringify(formattedOutput, null, 2)}`));
      // console.log(`-------------------`);
      response.header('Content-type', formatter.getContentType());
      if (formatter.getContentDisposition()) {
        response.header(
          'Content-Disposition',
          formatter.getContentDisposition(),
        );
      }
      response.send(formattedOutput);
      return response;
    } catch (e) {
      console.log(`getQuotePrice Catch Error: ${e}`);
      response.status(HttpStatus.BAD_REQUEST).send(
        new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: e.name + ' - ' + e.message,
          },
          HttpStatus.BAD_REQUEST,
        ),
      );
      throw response;
    }
    // return reject(e);
  }
}
