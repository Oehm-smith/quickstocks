import { BadRequestException, Controller, Get, Injectable, PipeTransform, Query, Res } from '@nestjs/common';
import { ServiceProvider } from '../services/serviceProviders/serviceProvider';
import { Period } from '../services/serviceProviders/serviceProvider.API';
import { ApiQuery } from '@nestjs/swagger';
import { OutFormat } from './QueryTypes';
// import colour from 'colors/safe';
import colours from 'colors';
import { FormatterService } from '../services/formatters/FormatterService';

colours.enable();

@Injectable()
export class OutFormatValidationPipe implements PipeTransform {
  transform(value: any): OutFormat {
    if (!value) {
      return OutFormat.json; // Default value
    }
    if (Array.isArray(value)) {
      // multiple format arguments given
      throw new BadRequestException(`OutFormatValidationPipe error - array received: ${JSON.stringify(value)} - please pass only one 'format' parameter`)
    }

    // Cleanup any possibly stray characaters from GET request
    value = value.replace(/[^\w\d]+/g, '');
    // Check if the value is one of the allowed formats and doesn't include a slash
    if (!Object.values(OutFormat).includes(value as OutFormat) || value.includes('/')) {
      throw new BadRequestException(
        `Invalid format "${value}". Allowed formats are: ${Object.values(OutFormat).join(', ')}.`,
      );
    }

    return value as OutFormat;
  }
}

@Injectable()
export class PeriodValidationPipe implements PipeTransform {
  transform(value: any): Period {
    if (!value) {
      return Period.d; // Default value
    }
    if (Array.isArray(value)) {
      // multiple format arguments given
      throw new BadRequestException(`OutFormatValidationPipe error - array received: ${JSON.stringify(value)} - please pass only one 'period' parameter`)
    }

    // Cleanup any possibly stray characaters from GET request
    value = value.replace(/[^\w\d]+/g, '');
    // Check if the value is one of the allowed formats and doesn't include a slash
    if (!Object.values(Period).includes(value as Period) || value.includes('/')) {
      throw new BadRequestException(
        `Invalid format "${value}". Allowed formats are: ${Object.values(OutFormat).join(', ')}.`,
      );
    }

    return value as Period;
  }
}

@Controller('history')
export class HistoryController {
  constructor(private readonly serviceProvider: ServiceProvider) {
  }

  /**
   * Perform symbol(s) history lookup.
   * @param params
   *  - symbols (comma delimited)
   *  - from: Date to start at or defaults to the beginning of stock history.  Can be 'today' and just values for today.
   *  - to: Date to end at or defaults to end of stock history  Can be 'today' and just values for today (either to or from causes this).
   *  - period: 'd'aily (default), 'w'eekly, 'm'onth or 'v' (dividends)
   *  - format: json (default), csv, csvPortfolio (allow to copy and paste in to my current spreadsheet as a solution until I get portfolio functionality working)
   */

  @Get()
  @ApiQuery({ name: 'symbols' })
  @ApiQuery({ name: 'from' })
  @ApiQuery({ name: 'to' })
  @ApiQuery({ name: 'period', enum: Period })
  @ApiQuery({ name: 'format', enum: OutFormat })
  async getHistory(
    @Query('symbols') symbols,
    @Query('from') from,
    @Query('to') to,
    @Query('period', new PeriodValidationPipe()) period: Period,
    @Query('format', new OutFormatValidationPipe()) format: OutFormat = OutFormat.json,
    @Res() response,
  ): Promise<any> {
    try {
      console.log(
        `Controller args - symbols: ${symbols}, from: ${from}, to: ${to}, period: ${period}, format: ${format}`,
      );

      // const query = 'amp.ax';
      // const queryOptions = { period1: '2021-05-08', period2: '2021-05-08', /* ... */ };
      // const result2 = await yahooFinance.historical(query, queryOptions);
      // console.log(`random result: ${JSON.stringify(result2)}`)
      // return;


      // const fixSymbols = (syms) => syms.split(/,/).map(s => s.replace(/[^\w\d^\.]+/g, '').trim())
      //
      // const symbolsArray = fixSymbols(symbols)

      // const period = string2Period(query.period);
      // const format = query.format || 'json';
      // return resolve(response.send('not yet'));
      const serviceHistoryMap = await this.serviceProvider.history(
        symbols,
        from,
        to,
        period,
      );
      // const serviceHistoryMap = YahooFinance2Utils.convertChartResulToServiceHistorical(result)

      // const debugFormatter = new DebugFormatter();
      // console.log(colours.bgCyan.black(`getLookup - symbols: ${JSON.stringify(symbols)}, from: ${from}, to: ${to}, period: ${period} ...\n`
      //   + `${debugFormatter.formatHistory(result)}`));
      // const formatter = FormatterFactory.formatter(format);
      // const formattedOutput = formatter.formatHistory(serviceHistoryMap);
      const formatterService = new FormatterService(format, period);
      const formattedOutput = formatterService.format(serviceHistoryMap);

      console.log(`${formattedOutput}\n\n`);
      // const out = (JSON.stringify(formattedOutput, null, 2) || "") as string
      console.log(`formattedOutput:`);
      console.log(`${formattedOutput.black.bgWhite}`);
      console.log(`-------------------`);
      response.header('Content-type', formatterService.getContentType());
      if (formatterService.getContentDisposition()) {
        response.header(
          'Content-Disposition',
          formatterService.getContentDisposition(),
        );
      }
      return response.send(formattedOutput);
    } catch (e) {
      console.log(`Error: ${e}`);
      // throw e;
      return response.status(400).send(`Error: ${e.message}`)
    }
  }

  /*async getHistory2(
    @Query('symbols') symbols,
    @Query('from') from,
    @Query('to') to,
    @Query('period') period: Period = Period.d,
    @Query('format') format: OutFormat = OutFormat.json,
    @Res() response,
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        console.log(
          `Controller args - symbols: ${symbols}, from: ${from}, to: ${to}, period: ${period}, format: ${format}`,
        );

        const symbolsArray = symbols.split(/,\W*!/);
        // const period = string2Period(query.period);
        // const format = query.format || 'json';
        // return resolve(response.send('not yet'));
        const result = await this.serviceProvider.history(
          symbolsArray,
          from,
          to,
          period,
        );
        // const debugFormatter = new DebugFormatter();
        // console.log(colours.bgCyan.black(`getLookup - symbols: ${JSON.stringify(symbols)}, from: ${from}, to: ${to}, period: ${period} ...\n`
        //   + `${debugFormatter.formatHistory(result)}`));
        const formatter = FormatterFactory.formatter(format);
        const formattedOutput = formatter.formatHistory(result);
        // console.log(colours.black.bgCyan(`${JSON.stringify(formattedOutput, null, 2)}`));
        // console.log(`-------------------`);
        response.header('Content-type', formatter.getContentType());
        if (formatter.getContentDisposition()) {
          response.header(
            'Content-Disposition',
            formatter.getContentDisposition(),
          );
        }
        return resolve(response.send(formattedOutput));
      } catch (e) {
        console.log(`Error: ${JSON.stringify(e)}`);
        return reject(e);
      }
    });
  }*/
}
