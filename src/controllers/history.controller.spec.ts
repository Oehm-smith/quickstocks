import * as sinon from 'sinon';
import { Test, TestingModule } from '@nestjs/testing';
import { TestHelper } from '../../test/testData/testHelper';
import { ServiceProvider } from '../services/serviceProviders/serviceProvider';
import { HistoryController } from './history.controller';
import { MockResponse } from '../utils/TestUtils';
import { OutFormat } from './QueryTypes';
import { afterEach, beforeEach, describe, expect, it } from '@jest/globals';
import { MapChartResultsArray } from '../services/serviceProviders/YahooFinance2/YahooHistory2.model';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import { YahooFinance2 } from '../services/serviceProviders/YahooFinance2/YahooFinance2';
import { YahooFinance2ServiceProvider } from '../services/serviceProviders/YahooFinance2/YahooFinance2.serviceProvider';

// import { Console } from 'console';
// import * as process from 'process';
//
// const g: any = typeof global !== 'undefined' ? global : window;
// g.console = new Console(process.stdout, process.stderr);

global.console = require('console'); // to turn off verbose intellij / webstore console.log

describe('HistoryController', () => {
  let historyController: HistoryController;
  let serviceProvider;
  let yahooFinanceServiceProvider;
  let yahooFinanceStub;
  let yahooFinance;
  let response;

  describe('cba', () => {
    beforeEach(async () => {
      // const app: TestingModule = await Test.createTestingModule({
      //   controllers: [LookupController],
      //   providers: [ServiceProvider, YahooFinanceServiceProvider, YahooFinance],
      // }).compile();
      //
      // lookupController = app.get<LookupController>(LookupController);

      // beforeEach(async () => {
      yahooFinance = new YahooFinance2();
      yahooFinanceStub = sinon
        .stub(yahooFinance, 'historical')
        .callsFake(
          (
            symbol: string,
            startDate: string,
            endDate: string,
          ): Promise<MapChartResultsArray> => {
            const file =
              './test/testData/YahooFinance/cba_2020-05-11_to_2020-05-12_ChartResultArray.json';
            const data = TestHelper.getAndPrepareTestData(
              file,
              startDate,
              endDate,
            ) as Promise<MapChartResultsArray>;
            return data;
          },
        );
      yahooFinanceServiceProvider = new YahooFinance2ServiceProvider(
        yahooFinance,
      );
      serviceProvider = new ServiceProvider(yahooFinanceServiceProvider);
      historyController = new HistoryController(serviceProvider);

      response = new MockResponse();
      // });
    });

    afterEach(async () => {
      yahooFinanceStub.restore();
    });

    describe('history', () => {
      it('/cba in json should return CommonHistorical in json format', async () => {
        try {
          await historyController.getHistory(
            'irrelevantInTestComesFromTestFileAsDoDates',
            null,
            null,
            null,
            OutFormat.json,
            response,
          );
          const stringResult = response.body;
          const jsonResult = JSON.parse(stringResult);
          console.log(`objJson: ${JSON.stringify(jsonResult, null, 2)}`);
          // expect(Array.isArray(jsonResult)).toBeTruthy();
          expect(jsonResult).toBeDefined();
          expect(typeof jsonResult).toEqual('object');
          const cba = jsonResult['CBA.AX'];
          expect(cba).toHaveLength(2);
          expect(cba[0].symbol).toEqual('CBA.AX');
          expect(cba[1].symbol).toEqual('CBA.AX');
        } catch (err) {
          console.log('ERRROR: ', err); // Only shows message
          throw err;
        }
      });
    });

    it('/cba in csvPortfolio format', async () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const jsonResult = await historyController.getHistory(
        'irrelevantInTestComesFromTestFileAsDoDates',
        null,
        null,
        null,
        OutFormat.csvPortfolio,
        response,
      );
    });
  });

  describe('cba, bhp', () => {
    beforeEach(async () => {
      yahooFinance = new YahooFinance2();
      yahooFinanceStub = sinon
        .stub(yahooFinance, 'historical')
        .callsFake(
          (
            symbol: string,
            startDate: string,
            endDate: string,
          ): Promise<MapChartResultsArray> => {
            const file =
              './test/testData/YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_MapChartResultsArray.json';
            return TestHelper.getAndPrepareTestData(
              file,
              startDate,
              endDate,
            ) as Promise<MapChartResultsArray>;
          },
        );
      yahooFinanceServiceProvider = new YahooFinance2ServiceProvider(
        yahooFinance,
      );
      serviceProvider = new ServiceProvider(yahooFinanceServiceProvider);
      historyController = new HistoryController(serviceProvider);
      // });
    });

    afterEach(async () => {
      yahooFinanceStub.restore();
    });

    describe('lookup', () => {
      it('/cba, bhp in json should return CommonHistoricalBatch in json format', async () => {
        try {
          const mockResponse = {
            status: jest.fn().mockReturnThis(), // Chainable method
            json: jest.fn(),
          };

          await historyController.getHistory(
            'irrelevantInTestComesFromTestFileAsDoDates',
            null,
            null,
            null,
            OutFormat.json,
            mockResponse as any,
          );

          expect(mockResponse.status).toHaveBeenCalledWith(200);
          console.log('mockResponse: ', mockResponse.json.mock.calls); // Prints the arguments passed to `json`

          const jsonResult = response.body;
          // const objJson = JSON.parse(jsonResult);
          // expect(Array.isArray(jsonResult)).toBeTruthy();
          expect(jsonResult).toBeDefined();
          expect(Object.keys(jsonResult)).toContain('cba.ax');
          expect(Object.keys(jsonResult)).toContain('bhp.ax');
          expect(Object.keys(jsonResult)).toContain('bhp.ax');
          expect(Array.isArray(jsonResult['cba.ax'])).toBeTruthy();
          expect(Array.isArray(jsonResult['bhp.ax'])).toBeTruthy();
          expect(jsonResult['cba.ax']).toHaveLength(2);
          expect(jsonResult['cba.ax'][0].symbol).toEqual('cba.ax');
          expect(jsonResult['cba.ax'][1].symbol).toEqual('cba.ax');
          expect(jsonResult['bhp.ax']).toHaveLength(2);
          expect(jsonResult['bhp.ax'][0].symbol).toEqual('bhp.ax');
          expect(jsonResult['bhp.ax'][1].symbol).toEqual('bhp.ax');
        } catch (error) {
          console.error('unexpected error: ', error);
        }
      });
    });

    it('/cba, bhp in csvPortfolio format', async () => {
      try {
        await historyController.getHistory(
          'irrelevantInTestComesFromTestFileAsDoDates',
          null,
          null,
          null,
          OutFormat.csvPortfolio,
          response,
        );
        const csvResult = response.body;
        console.log(`${JSON.stringify(csvResult, null, 2)}`);
        const rowResults = csvResult.split('\n');
        expect(rowResults[0]).toEqual('date,,CBA.AX,,BHP.AX,,');
        expect(rowResults[1]).toEqual('2020-05-12,,59.709999,,30.719999,');
        expect(rowResults[2]).toEqual('2020-05-11,,60.139999,,31.549999,');
      } catch (err) {
        console.log('ERRROR: ', err); // Only shows message
        throw err;
      }
    });
  });

  describe('No test file - hit the backend without test file', () => {
    beforeEach(async () => {
      const app: TestingModule = await Test.createTestingModule({
        controllers: [HistoryController],
        providers: [ServiceProvider, YahooFinance2ServiceProvider, YahooFinance2],
      }).compile();

      historyController = app.get<HistoryController>(HistoryController);
    });

    it('cba, bhp', async () => {
      const csvResult = await historyController.getHistory(
        '^axjo,bhp.ax,cba.ax',
        '2020-05-27',
        '2020-05-29',
        null,
        OutFormat.csvPortfolio,
        response,
      );
      console.log(csvResult);
    });

    it('full portfolio', async () => {
      await historyController.getHistory(
        '^axjo,amp.ax,bhp.ax,bkl.ax,bvs.ax,cba.ax,flt.ax,fxl.ax,hvn.ax,llc.ax,ori.ax,qbe.ax,s32.ax,sgp.ax,sol.ax,sul.ax,twe.ax,web.ax,wmi.ax,wor.ax',
        '2020-05-27',
        '2020-05-29',
        null,
        OutFormat.csvPortfolio,
        response,
      );
      const csvResult = response.body;
      console.log(csvResult);
    });
  });
  describe('^axjo, cba, bhp', () => {
    beforeEach(async () => {
      yahooFinance = new YahooFinance2();
      yahooFinanceStub = sinon
        .stub(yahooFinance, 'historicalBatch')
        .callsFake(
          (
            symbol: string,
            startDate: string,
            endDate: string,
          ): Promise<ChartResultArray | MapChartResultsArray> => {
            const file =
              './test/testData/YahooFinance/axjo_cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json';
            return TestHelper.getAndPrepareTestData(
              file,
              startDate,
              endDate,
            ) as Promise<ChartResultArray | MapChartResultsArray>;
          },
        );
      yahooFinanceServiceProvider = new YahooFinance2ServiceProvider(
        yahooFinance,
      );
      serviceProvider = new ServiceProvider(yahooFinanceServiceProvider);
      historyController = new HistoryController(serviceProvider);
      // });
    });

    afterEach(async () => {
      yahooFinanceStub.restore();
    });

    it('^axjo, /cba, bhp in csvPortfolio format', async () => {
      await historyController.getHistory(
        'irrelevantInTestComesFromTestFileAsDoDates',
        null,
        null,
        null,
        OutFormat.csvPortfolio,
        response,
      );
      const csvResult = response.body;
      console.log(csvResult);
      const rowResults = csvResult.split('\n');
      expect(rowResults[0]).toEqual('date,^AXJO,,CBA.AX,,BHP.AX');
      expect(rowResults[1]).toEqual('2020-05-12,5403,,59.709999,,30.719999,');
      expect(rowResults[2]).toEqual(
        '2020-05-11,5461.200195,,60.139999,,31.549999,',
      );
    });
  });
});
