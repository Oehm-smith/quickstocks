import { Test, TestingModule } from '@nestjs/testing';
import { afterEach, beforeEach, describe, expect, it, jest } from '@jest/globals';
import { HistoryController, OutFormatValidationPipe } from './history.controller';
import { BadRequestException } from '@nestjs/common';
import { Period } from '../services/serviceProviders/serviceProvider.API';
import { OutFormat } from './QueryTypes';
import { ServiceHistoricalBatch } from '../services/serviceProviders/ServiceProviderHistory.model';

// Mock dependencies
const mockServiceProvider = {
  history: jest.fn<Promise<ServiceHistoricalBatch>, any[]>(),
};

const mockFormatterService = {
  format: jest.fn(),
  getContentType: jest.fn(),
  getContentDisposition: jest.fn(),
};

describe('HistoryController', () => {
  let controller: HistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistoryController],
      providers: [
        { provide: 'ServiceProvider', useValue: mockServiceProvider },
      ],
    }).compile();

    controller = module.get<HistoryController>(HistoryController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('getHistory', () => {
    it('should return formatted history data for valid query parameters', async () => {
      const mockResponse = {
        header: jest.fn(),
        send: jest.fn(),
      };
      const mockData = { data: 'test data' };
      const formattedOutput = 'formatted data';

      mockServiceProvider.history.mockResolvedValue(mockData);
      mockFormatterService.format.mockReturnValue(formattedOutput);
      mockFormatterService.getContentType.mockReturnValue('application/json');
      mockFormatterService.getContentDisposition.mockReturnValue(null);

      const formatterSpy = jest
        .spyOn(global, 'FormatterService')
        .mockImplementation(() => mockFormatterService);

      await controller.getHistory(
        'AAPL',
        '2023-01-01',
        '2023-12-31',
        Period.d,
        OutFormat.json,
        mockResponse as any,
      );

      expect(mockServiceProvider.history).toHaveBeenCalledWith(
        'AAPL',
        '2023-01-01',
        '2023-12-31',
        Period.d,
      );

      expect(mockFormatterService.format).toHaveBeenCalledWith(mockData);
      expect(mockResponse.header).toHaveBeenCalledWith('Content-type', 'application/json');
      expect(mockResponse.send).toHaveBeenCalledWith(formattedOutput);

      formatterSpy.mockRestore();
    });

    it('should set Content-Disposition header if provided by FormatterService', async () => {
      const mockResponse = {
        header: jest.fn(),
        send: jest.fn(),
      };
      const mockData = { data: 'test data' };
      const formattedOutput = 'formatted data';

      mockServiceProvider.history.mockResolvedValue(mockData);
      mockFormatterService.format.mockReturnValue(formattedOutput);
      mockFormatterService.getContentType.mockReturnValue('application/json');
      mockFormatterService.getContentDisposition.mockReturnValue('attachment; filename="test.csv"');

      const formatterSpy = jest
        .spyOn(global, 'FormatterService')
        .mockImplementation(() => mockFormatterService);

      await controller.getHistory(
        'AAPL',
        '2023-01-01',
        '2023-12-31',
        Period.d,
        OutFormat.json,
        mockResponse as any,
      );

      expect(mockResponse.header).toHaveBeenCalledWith(
        'Content-Disposition',
        'attachment; filename="test.csv"',
      );

      formatterSpy.mockRestore();
    });

    it('should handle service errors gracefully', async () => {
      const mockResponse = { send: jest.fn() };

      mockServiceProvider.history.mockRejectedValue(new Error('Service error'));

      await expect(
        controller.getHistory(
          'AAPL',
          '2023-01-01',
          '2023-12-31',
          Period.d,
          OutFormat.json,
          mockResponse as any,
        ),
      ).rejects.toThrow('Service error');
    });

    it('should throw BadRequestException for invalid format', async () => {
      const mockResponse = { send: jest.fn() };
      const invalidFormat = 'invalid';

      jest.spyOn(OutFormatValidationPipe.prototype, 'transform').mockImplementation(() => {
        throw new BadRequestException('Invalid format');
      });

      await expect(
        controller.getHistory(
          'AAPL',
          '2023-01-01',
          '2023-12-31',
          Period.d,
          invalidFormat as OutFormat,
          mockResponse as any,
        ),
      ).rejects.toThrow(BadRequestException);
    });
  });
});
