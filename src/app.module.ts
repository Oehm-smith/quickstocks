import { Module } from '@nestjs/common';
import { AppService } from './services/app.service';
import { ServiceProvider } from './services/serviceProviders/serviceProvider';
import { HistoryController } from './controllers/history.controller';
import { QuoteController } from './controllers/quote.controller';
import { YahooFinance2ServiceProvider } from './services/serviceProviders/YahooFinance2/YahooFinance2.serviceProvider';
import { YahooFinance2 } from './services/serviceProviders/YahooFinance2/YahooFinance2';
import { ServiceProviderAPI_TOKEN } from './services/serviceProviders/serviceProvider.API';

@Module({
  imports: [],
  controllers: [HistoryController, QuoteController],
  providers: [
    AppService,
    ServiceProvider,
    {
      provide: ServiceProviderAPI_TOKEN,
      useClass: YahooFinance2ServiceProvider,
    },
    YahooFinance2,
    {
      provide: ServiceProviderAPI_TOKEN,
      useClass: YahooFinance2ServiceProvider,
    },
  ],
})
export class AppModule {
}
