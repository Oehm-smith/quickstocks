export class ObjectUtils {
  static fixKey(key) {
    // return key.replace(/^ */, '').replace(/^\d+\.?\d* */, '').replace(/ *\. */, '')
    const upperCaseReplacer = (match: string, g1: string) => {
      // console.log(`upperCaseReplacer replacer - m: ${match}, g1: ${g1}`)
      return g1.toUpperCase();
    };
    const wordsReplacer = (match: string, g1: string, g2: string) => {
      return g1 + g2.toUpperCase();
    };
    const m1 = key.replace(/^\d+ *\. *([a-zA-Z])/, upperCaseReplacer);
    const m2 = m1.replace(/([a-zA-Z]) +\(?([a-zA-Z])/g, wordsReplacer);
    const m3 = m2.replace(/^([a-zA-Z])/, upperCaseReplacer);
    const out = m3.replace(/[()]/, '');
    // console.log(`fixKey - in: ${key}, m1: ${m1}, out: ${out}`)
    return out;
  }

  static fixObj(obj: any) {
    let newObj = null;
    if (Array.isArray(obj)) {
      newObj = [];
      for (const i of obj) {
        newObj.push(ObjectUtils.fixObj(i));
      }
      return newObj;
    } else if (typeof obj === 'object') {
      newObj = {};
      for (const key of Object.keys(obj)) {
        newObj[ObjectUtils.fixKey(key)] = ObjectUtils.fixObj(obj[key]);
      }
      return newObj;
    } else {
      return obj;
    }
  }
}
