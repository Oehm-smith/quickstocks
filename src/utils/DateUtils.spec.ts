// Import the DateUtils.sortDates function and SortOrder enum (if necessary)
// import { DateUtils.sortDates, SortOrder } from './path-to-your-file';
import { describe, expect, it } from '@jest/globals';
import { DateUtils } from './DateUtils';
import { SortOrder } from '../services/serviceProviders/YahooFinance2/YahooHistory2.model';

describe('DateUtils', () => {
  describe('DateUtils.sortDates', () => {
    it('should sort dates in ascending order', () => {
      const dates = [
        new Date('2024-12-31'),
        new Date('2024-01-01'),
        new Date('2024-06-15'),
      ];

      const sortedDates = DateUtils.sortDates(dates, SortOrder.ascending);

      expect(sortedDates).toEqual([
        new Date('2024-01-01'),
        new Date('2024-06-15'),
        new Date('2024-12-31'),
      ]);
    });

    it('should sort dates in descending order', () => {
      const dates = [
        new Date('2024-12-31'),
        new Date('2024-01-01'),
        new Date('2024-06-15'),
      ];

      const sortedDates = DateUtils.sortDates(dates, SortOrder.descending);

      expect(sortedDates).toEqual([
        new Date('2024-12-31'),
        new Date('2024-06-15'),
        new Date('2024-01-01'),
      ]);
    });

    it('should handle an empty array', () => {
      const dates: Date[] = [];
      const sortedDates = DateUtils.sortDates(dates, SortOrder.ascending);
      expect(sortedDates).toEqual([]);
    });

    it('should handle dates with the same values', () => {
      const dates = [
        new Date('2024-06-15'),
        new Date('2024-06-15'),
        new Date('2024-06-15'),
      ];

      const sortedDates = DateUtils.sortDates(dates, SortOrder.ascending);

      expect(sortedDates).toEqual([
        new Date('2024-06-15'),
        new Date('2024-06-15'),
        new Date('2024-06-15'),
      ]);
    });

    it('should handle a single date in the array', () => {
      const dates = [new Date('2024-06-15')];
      const sortedDates = DateUtils.sortDates(dates, SortOrder.ascending);
      expect(sortedDates).toEqual([new Date('2024-06-15')]);
    });
  });

  describe('DateUtils.DateUtils.convertToDate', () => {
    it('should convert an array of valid numbers to Date objects', () => {
      const numbers = [1672531200000, 1672617600000, 1672704000000]; // Epoch milliseconds for valid dates

      const dates = DateUtils.convertToDate(numbers);

      expect(dates).toEqual([
        new Date(1672531200000),
        new Date(1672617600000),
        new Date(1672704000000),
      ]);
    });

    it('should convert a single valid number to a Date object', () => {
      const number = 1672531200000; // Epoch milliseconds for a valid date

      const date = DateUtils.convertToDate(number);

      expect(date).toEqual(new Date(1672531200000));
    });

    it('should throw an error if any number in an array is not an integer', () => {
      const invalidNumbers = [1672531200000, 1672617600000, 1672704000.5];

      expect(() => DateUtils.convertToDate(invalidNumbers)).toThrowError(
        'DateUtils.convertToDate - is not a number: 1672704000.5',
      );
    });

    it('should throw an error if a single number is not an integer', () => {
      const invalidNumber = 1672704000.5;

      expect(() => DateUtils.convertToDate(invalidNumber)).toThrowError(
        'DateUtils.convertToDate - is not a number: 1672704000.5',
      );
    });

    it('should throw an error if a number in an array cannot be converted to a valid Date', () => {
      const invalidDates = [NaN, Infinity, -1];

      invalidDates.forEach((n) => {
        expect(() => DateUtils.convertToDate([n])).toThrowError(
          `DateUtils.convertToDate - invalid date from number: ${n}`,
        );
      });
    });

    it('should throw an error if a single number cannot be converted to a valid Date', () => {
      const invalidDate = NaN;

      expect(() => DateUtils.convertToDate(invalidDate)).toThrowError(
        `DateUtils.convertToDate - invalid date from number: ${invalidDate}`,
      );
    });

    it('should handle an empty array', () => {
      const numbers: number[] = [];
      const dates = DateUtils.convertToDate(numbers);
      expect(dates).toEqual([]);
    });
  });
});