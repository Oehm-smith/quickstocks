import { ObjectUtils } from './ObjectUtils';
import { describe, expect, it } from '@jest/globals';

describe('ObjectUtils', () => {
  describe('fixObj', () => {
    it('simple', () => {
      const data = [
        'a',
        'b',
        {
          c1: 'hello',
          '1. blah': 'blah',
          next: {
            '2. test': 'testing',
            '3. done': 'doneth',
            '20-07-18': 'this is a date',
            '1. spaces here': 'need to exist',
            'spaces here': 'need to exist',
            'weekly time series': 'need to come together',
            'TimeSeries (Daily)': 'different variation',
          },
        },
      ];
      const out = ObjectUtils.fixObj(data);
      console.log(`simple in: ${JSON.stringify(data, null, 2)}`);
      console.log(`simple out: ${JSON.stringify(out, null, 2)}`);

      expect(Array.isArray(out)).toEqual(true);
      expect(out[0]).toEqual('a');
      expect(typeof out[2]).toEqual('object');
      expect(Object.keys(out[2])).toContain('C1');
      expect(Object.keys(out[2])).toContain('Blah');
      expect(typeof Object.keys(out[2].Next)).toEqual('object');
      expect(Object.keys(out[2].Next)).toContain('Test');
      expect(Object.keys(out[2].Next)).toContain('Done');
      expect(Object.keys(out[2].Next)).toContain('20-07-18');
      expect(out[2].Next.Test).toEqual('testing');
      expect(out[2].Next.Done).toEqual('doneth');
      expect(Object.keys(out[2].Next)).toContain('SpacesHere');
      expect(Object.keys(out[2].Next)).toContain('WeeklyTimeSeries');
      expect(Object.keys(out[2].Next)).toContain('TimeSeriesDaily');
    });
  });

  it('test proper data', () => {
    const data = {
      'Meta Data': {
        '1. Information': 'Weekly Prices (open, high, low, close) and Volumes',
        '2. Symbol': 'cba.ax',
        '3. Last Refreshed': '2019-05-17',
        '4. Time Zone': 'US/Eastern',
      },
      'Weekly Time Series': {
        '2019-05-17': {
          '1. open': '73.9000',
          '2. high': '74.0000',
          '3. low': '72.0150',
          '4. close': '72.8300',
          '5. volume': '15441431',
        },
        '2019-05-10': {
          '1. open': '74.4700',
          '2. high': '75.6500',
          '3. low': '74.1500',
          '4. close': '75.4000',
          '5. volume': '8983069',
        },
      },
    };

    const out = ObjectUtils.fixObj(data);
    console.log(`test 2 - in: ${JSON.stringify(data, null, 2)}`);
    console.log(`test 2 - out: ${JSON.stringify(out, null, 2)}`);

    expect(Object.keys(out)).toContain('MetaData');
    expect(Object.keys(out)).toContain('WeeklyTimeSeries');

    expect(Object.keys(out.MetaData)).toContain('Information');
    expect(Object.keys(out.MetaData)).toContain('Symbol');
    expect(Object.keys(out.MetaData)).toContain('LastRefreshed');
    expect(Object.keys(out.MetaData)).toContain('TimeZone');

    expect(Object.keys(out.WeeklyTimeSeries)).toContain('2019-05-17');
    expect(Object.keys(out.WeeklyTimeSeries)).toContain('2019-05-10');

    expect(Object.keys(out.WeeklyTimeSeries['2019-05-17'])).toContain('Open');
    expect(Object.keys(out.WeeklyTimeSeries['2019-05-17'])).toContain('High');
    expect(Object.keys(out.WeeklyTimeSeries['2019-05-17'])).toContain('Low');
    expect(Object.keys(out.WeeklyTimeSeries['2019-05-17'])).toContain('Close');
    expect(Object.keys(out.WeeklyTimeSeries['2019-05-17'])).toContain(
      'Volume',
    );

    expect(out.WeeklyTimeSeries['2019-05-17'].Volume).toEqual('15441431');
  });
});
