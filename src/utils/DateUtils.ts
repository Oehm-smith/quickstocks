import { SortOrder } from '../services/serviceProviders/YahooFinance2/YahooHistory2.model';

export class DateUtils {

  static sortDates = (dates: Date[], order: SortOrder = SortOrder.ascending): Date[] => {
    const orderFunction = () => {
      const compareDates = (a: Date, b: Date): number => {
        if (a.getFullYear() !== b.getFullYear()) {
          return b.getFullYear() - a.getFullYear();
        }
        if (a.getMonth() !== b.getMonth()) {
          return b.getMonth() - a.getMonth();
        }
        return b.getDate() - a.getDate();
      };

      return function(p1: Date, p2: Date) {
        switch (order) {
          case SortOrder.ascending:
            return compareDates(p2, p1);
            break;
          case SortOrder.descending:
          default:
            return compareDates(p1, p2);
        }
      };
    };

    return dates.sort(orderFunction());
  };

  static isSorted = (dates: Date[], order: SortOrder = SortOrder.ascending): boolean => {
    for (let i = 1; i < dates.length; i++) {
      if (order === SortOrder.ascending && dates[i] < dates[i - 1]) {
        return false; // Not in ascending order
      }
      if (order === SortOrder.descending && dates[i] > dates[i - 1]) {
        return false; // Not in descending order
      }
    }
    return true;
  };

  static areDateListsEqual = (dates1: Date[], dates2: Date[] | number[], sortOrder: SortOrder, careForOrder: boolean = false): boolean => {
    if (Number.isInteger(dates2)) {
      dates2 = dates2.map(d => new Date(d as number) as Date);
    }
    if (dates1.length !== dates2.length) {
      console.error(`Date lists are not equal - lengths differ: ${JSON.stringify(dates1)}, ${JSON.stringify(dates2)}`);
      return false; // If lengths differ, they can't be equal
    }
    // if (!DateUtils.isSorted(dates1, sortOrder)) {
    //   console.error(`Dates1 are not sorted: ${JSON.stringify(dates1)}`);
    //   return false;
    // }
    // if (!DateUtils.isSorted(dates2 as Date[], sortOrder)) {
    //   console.error(`Dates2 are not sorted: ${JSON.stringify(dates2)}`);
    //   return false;
    // }

    let dates11 = dates1;
    let dates21 = dates2;
    if (careForOrder) {
      dates11 = DateUtils.sortDates(dates1);
      dates21 = DateUtils.sortDates(dates2 as Date[]);
    }
    return dates11.every((date, index) => date === dates21[index]);
  };

  static convertToDate(numbers: number[] | number): Date[] | Date {
    const doConvert = (num: number) => {
      if (isNaN(num) || !isFinite(num) || num < 0) {
        throw Error(`DateUtils.convertToDate - invalid date from number: ${num}`);
      }
      if (!Number.isInteger(num)) {
        throw Error(`DateUtils.convertToDate - is not a number: ${num}`);
      }
      return new Date(num);
    };

    if (Array.isArray(numbers)) {
      const dates = [];

      for (const n of numbers) {
        dates.push(doConvert(n));
      }
      return dates;
    } else {
      return doConvert(numbers);
    }
  }
}