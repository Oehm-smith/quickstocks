import { HttpStatus } from '@nestjs/common';
import { ServiceHistorical, ServiceHistoricalBatch } from '../services/serviceProviders/ServiceProviderHistory.model';

export class MockResponse {
  body: any = null;
  headers: Map<string, string> = new Map<string, string>();
  httpStatus: HttpStatus;

  send(body?: any) {
    this.body = body;
    return this;
  }

  status(status: HttpStatus) {
    this.httpStatus = status;
    return this;
  }

  header(name: string, value: string) {
    this.headers.set(name, value);
    return this;
  }
}

function objectToMap<K extends string, V>(input: Record<K, V>): Map<K, V> {
  return new Map<K, V>(Object.entries(input) as [K, V][]);
}

export class DataHelpers {
  static getFileServiceHistoryDataPoints(file: string): (ServiceHistorical | ServiceHistoricalBatch) {
    const data = require(`@dataFiles/${file}`);

    const fixDates = (chartData: any): ServiceHistorical => {
      for (const data of chartData) {
        const theDate = new Date(data.date);
        data.date = theDate;
      }
      return chartData;
    };

    const fixDatesInBatch = (serviceHistoricalBatch: Map<string, ServiceHistorical>) => {
      const symbols = [...serviceHistoricalBatch.keys()];
      for (const s of symbols) {
        const data = serviceHistoricalBatch.get(s);
        fixDates(data);
      }
    };

    if (Array.isArray(data)) {
      fixDates(data);
      return data;
    } else {
      // return new Map<string, ServiceHistorical>(Object.entries(data)) as ServiceHistoricalBatch
      const serviceHistoricalBatch = objectToMap<string, ServiceHistorical>(data);
      fixDatesInBatch(serviceHistoricalBatch);
      return serviceHistoricalBatch;
    }
  }

}