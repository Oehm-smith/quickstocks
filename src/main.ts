import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as process from 'process';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Quick Stocks')
    .setDescription('API description for Quick Stocks service')
    .setVersion('1.0')
    // .addTag('portfolio').addTag('shares').addTag('stocks')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  const PORT = process.env.PORT || 9005;
  console.log(`QuoteStocks started - listening on port ${PORT}`);

  await app.listen(PORT);
}

bootstrap();
