import moment from 'moment';
import { beforeEach, describe, expect, jest, test } from '@jest/globals';
import { CSVPortfolioFormatter } from './CSVPortfolioFormatter';
import { ServiceHistoricalBatch, ServiceHistoryDataPoint } from '../serviceProviders/ServiceProviderHistory.model';
import { YahooFinance2Utils } from '../serviceProviders/YahooFinance2/YahooFinance2.utils';
import { SortOrder } from '../serviceProviders/YahooFinance2/YahooHistory2.model';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

describe('CSVPortfolioFormatter', () => {
  let formatter: CSVPortfolioFormatter;

  beforeEach(() => {
    formatter = new CSVPortfolioFormatter('CSVPortfolio');
  });

  test('getContentType should return correct content type', () => {
    expect(formatter.getContentType()).toBe('text/csv');
  });

  test('getContentDisposition should return formatted filename', () => {
    const mockDate = '2023-01-01_12:00';
    jest.spyOn(moment.prototype, 'format').mockReturnValue(mockDate);

    const result = formatter.getContentDisposition();
    expect(result).toBe(`attachment; filename=quickStocks-lookup-${mockDate}.csv`);
  });

  test('formatHeader should return correctly formatted header', () => {
    const symbols = ['^AXJO', 'CBA.AX', 'BHP.AX'];
    const result = formatter['formatHeader'](symbols);

    const expectedHeader = 'date,^AXJO,,CBA.AX,,,BHP.AX,,,';
    expect(result).toBe(expectedHeader);
  });

  test('formatData should return adjusted close as string', () => {
    const mockData: ServiceHistoryDataPoint = {
      date: new Date('2023-01-01'),
      open: 10,
      high: 15,
      low: 8,
      close: 12,
      volume: 1000,
      adjClose: 12.5,
      symbol: '^AXJO',
    };

    const result = formatter['formatData'](mockData);
    expect(result).toBe('12.5');
  });

  test('formatDate should return correctly formatted row for a date', () => {
    const mockData: ServiceHistoryDataPoint[] = [
      {
        date: new Date('2023-01-01'),
        open: 10,
        high: 15,
        low: 8,
        close: 12,
        volume: 1000,
        adjClose: 12.5,
        symbol: '^AXJO',
      },
    ];

    const asDate = new Map<number, ServiceHistoryDataPoint[]>();
    asDate.set(new Date('2023-01-01').getTime(), mockData);

    const result = formatter['formatDatedDate'](new Date('2023-01-01').getTime(), asDate);
    expect(result).toBe('01/01/2023,12.5,,');
  });

  test('format should return formatted CSV string', () => {
    const mockData: ServiceHistoricalBatch = new Map([
      [
        '^AXJO',
        [
          {
            date: new Date('2023-01-01'),
            open: 10,
            high: 15,
            low: 8,
            close: 12,
            volume: 1000,
            adjClose: 12.5,
            symbol: '^AXJO',
          },
        ],
      ],
      [
        'CBA.AX',
        [
          {
            date: new Date('2023-01-01'),
            open: 50,
            high: 55,
            low: 48,
            close: 52,
            volume: 2000,
            adjClose: 52.5,
            symbol: 'CBA.AX',
          },
        ],
      ],
    ]);

    const result = formatter.format(mockData);
    expect(result).toContain('date,^AXJO,,CBA.AX,,,');
    expect(result).toContain('1/1/2023,12.5,,52.5,,,');
  });

  test('YahooFinance2Utils should return correct unique symbols', () => {
    const mockData: ServiceHistoricalBatch = new Map([
      ['^AXJO', []],
      ['CBA.AX', []],
      ['BHP.AX', []],
    ]);
    const symbols = YahooFinance2Utils.getUniqueSymbols(mockData);
    expect(symbols).toEqual(['^AXJO', 'CBA.AX', 'BHP.AX']);
  });

  test('YahooFinance2Utils should aggregate data by date', () => {
    const mockData: ServiceHistoricalBatch = new Map([
      [
        '^AXJO',
        [
          {
            date: new Date('2023-01-01'),
            open: 10,
            high: 15,
            low: 8,
            close: 12,
            volume: 1000,
            adjClose: 12.5,
            symbol: '^AXJO',
          },
        ],
      ],
      [
        'CBA.AX',
        [
          {
            date: new Date('2023-01-01'),
            open: 50,
            high: 55,
            low: 48,
            close: 52,
            volume: 2000,
            adjClose: 52.5,
            symbol: 'CBA.AX',
          },
        ],
      ],
    ]);

    const aggregatedData = YahooFinance2Utils.aggregateByDate(mockData, SortOrder.descending);
    const unixTimeKey = new Date('2023-01-01').getTime();

    expect(aggregatedData.size).toBe(1);
    expect(aggregatedData.get(unixTimeKey)?.length).toBe(2);
  });
});
