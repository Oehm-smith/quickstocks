import { JSONFormatter } from './JSONFormatter';
import { CSVPortfolioFormatter } from './CSVPortfolioFormatter';
import { FormatterApi } from './Formatter.api';
import { Period } from '../serviceProviders/serviceProvider.API';

export class FormatterFactory {
  static formatter(format: string, period: Period): FormatterApi {
    const which = format.toLocaleLowerCase();
    console.log(`formatter - ${which}`);
    switch (which) {
      case 'csvportfolio':
        return new CSVPortfolioFormatter(which, period);
      case 'json':
      default:
        return new JSONFormatter(which, period);
    }
  }
}
