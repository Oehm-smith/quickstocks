import { FormatterApi } from './Formatter.api';
import {
  ServiceHistorical,
  ServiceHistoricalBatch,
  ServiceHistoryDataPoint,
} from '../serviceProviders/ServiceProviderHistory.model';
import {
  ServiceQuote,
  ServiceQuoteBatch,
  ServiceQuotePrice,
  ServiceQuoteSummaryDetail,
} from '../serviceProviders/ServiceProviderQuote.model';

const returnMapString = (map: Map<string, any>): string => {
  const mapString = [];
  map.forEach((value, key) => {
    mapString.push(`${key} => ${JSON.stringify(value)}`);
  });
  return mapString.join(',\n    ');
};

export class DebugFormatter extends FormatterApi {
  getContentType(): string {
    return 'text/text';
  }

  /**
   * To be returned in the response.  Optional - return null or '' or undefined if not used.
   */
  getContentDisposition(): string {
    return undefined;
  }

  formatHistory(
    item: ServiceHistoryDataPoint[] | Map<string, ServiceHistorical>,
  ): any {
    if ((item as ServiceHistoricalBatch).entries()) {
      return this.formatHistoricalBatch(item as ServiceHistoricalBatch);
    } else {
      return this.formatHistorical(item as ServiceHistorical);
    }
  }

  formatHistorical(historical: ServiceHistorical): string {
    const dps = ['    '];

    historical.forEach((h) => {
      dps.push(`date: ${h.date}, close: ${h.close}, symbol: ${h.symbol}`);
    });
    return `[DataPoints - ${dps.join('\n')}]\n`;
  }

  formatHistoricalBatch(historicalBatch: Map<string, ServiceHistorical>): any {
    return (
      `[YahooFinance HistoricalBatch - \n` +
      `    ${returnMapString(historicalBatch)}\n` +
      `]`
    );
  }

  formatQuote(data: ServiceQuote | ServiceQuoteBatch): any {
    if ((data as ServiceQuoteBatch).entries()) {
      return this.formatQuoteBatch(data as ServiceQuoteBatch);
    } else {
      return this.formatQuoteItem(data as ServiceQuote);
    }
  }

  formatQuoteItem(quote: ServiceQuote): any {
    const dps = ['    '];

    dps.push(this.formatQuoteItemPrice(quote.price));
    dps.push(this.formatQuoteItemSummaryDetail(quote.summaryDetail));
    //`date: ${h.date}, close: ${h.close}, symbol: ${h.symbol}`);
    return `[Quote - ${dps.join('\n')}]\n`;
  }

  formatQuoteBatch(data: Map<string, ServiceQuote>): any {
    return (
      `[YahooFinance HistoricalBatch - \n` +
      `    ${returnMapString(data)}\n` +
      `]`
    );
  }

  private formatQuoteItemPrice(price: ServiceQuotePrice) {
    return '[ServiceQuotePrice: ' + price
      ? JSON.stringify(price)
      : 'empty' + ']';
  }

  private formatQuoteItemSummaryDetail(
    summaryDetail: ServiceQuoteSummaryDetail,
  ) {
    return '[ServiceQuoteSummaryDetail: ' + summaryDetail
      ? JSON.stringify(summaryDetail)
      : 'empty' + ']';
  }

  format(data: ServiceHistoricalBatch): any {
  }
}
