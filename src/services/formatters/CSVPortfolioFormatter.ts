import moment from 'moment';
// import moment = require('moment');
import { FormatterApi } from './Formatter.api';
import {
  ServiceHistorical,
  ServiceHistoricalBatch,
  ServiceHistoricalBatchByDate,
  ServiceHistoryDataPoint,
} from '../serviceProviders/ServiceProviderHistory.model';
import { ServiceQuote, ServiceQuoteBatch } from '../serviceProviders/ServiceProviderQuote.model';
import { YahooFinance2Utils } from '../serviceProviders/YahooFinance2/YahooFinance2.utils';
import { SortOrder } from '../serviceProviders/YahooFinance2/YahooHistory2.model';
import { Period } from '../serviceProviders/serviceProvider.API';

const formatter = new Intl.DateTimeFormat('en-AU', { day: '2-digit', month: '2-digit', year: 'numeric' });

/**
 * This formatter is for my portfolio that lists shares in my portfolio in an excel file.  The cols are:
 *    Date,  ASX 200,  AMP  (Shares, price),  BHP (Shares, price), ....<more shares>
 * The formatted doesn't know about the share symbols - It just outputs the 2 cols in the order given (the 'shares' column should be empty and
 * this data should be merged in (with an 'add' i think).
 */
export class CSVPortfolioFormatter extends FormatterApi {
  getContentType(): string {
    return 'text/csv';
  }

  /**
   * To be returned in the response.  Optional - return null or '' or undefined if not used.
   */
  getContentDisposition(): string {
    // const m = Moment();
    return `attachment; filename=quickStocks-lookup-${moment().format('YYYY-MM-DD_HH:mm')}.csv`;
  }

  /** Historical *********************************************/

  private formatHeader(symbols: string[]): string {
    const row = [];
    row.push('date,');
    for (const s of symbols) {
      row.push(s);
      if (this.isIndexFund(s)) {
        row.push(',,');
      } else {
        row.push(',,,');
      }
    }
    return row.join('');
  }

  private formatData(d: ServiceHistoryDataPoint): string {
    const val = d.adjClose ? d.adjClose : d.close ? d.close : '-1';
    return val.toString(10);
  }

  private formatDatedDate(date: number, asDate: ServiceHistoricalBatchByDate): string {
    const dateData = asDate.get(date);
    const row = [];
    row.push(formatter.format(new Date(date)));
    row.push(',');
    for (const d of dateData) {
      row.push(this.formatData(d));
      if (this.isIndexFund(d.symbol)) {
        row.push(',,');
      } else {
        row.push(',,,');
      }
    }
    return row.join('');
  }

  format(data: ServiceHistoricalBatch): any {
    console.log(`CSVPortfolioFormatter\n\n`);
    const symbols = YahooFinance2Utils.getUniqueSymbols(data);
    const asDate = YahooFinance2Utils.aggregateByDate(data, SortOrder.ascending);
    const rows = [];
    const header = this.formatHeader(symbols);
    rows.push(header);
    for (const date of [...asDate.keys()]) {
      let includeThis = true
      if (this.period == Period.w) {
        const d = new Date(date);
        if (d.getDay() != 5) {
          // Only print Fridays when period is weekly
          includeThis = false;
        }
      }
      if (includeThis) {
        console.log(`format output by date: ${date}`)
        const dateRow = this.formatDatedDate(date, asDate);
        rows.push(dateRow)
      }
    }
    const result = rows.join('\n');
    return result;

    // if (Array.isArray(data)) {
    //   return this.formatHistorical(data as ServiceHistorical);
    // } else {
    //   return this.formatHistoricalBatch(data as ServiceHistoricalBatch);
    // }
  }

  formatHistorical(historical: ServiceHistorical): any {
  }

  formatHistoricalBatch(historicalBatch: ServiceHistoricalBatch): any {
  }

  formatQuote(data: ServiceQuote | ServiceQuoteBatch): any {
  }

  formatQuoteBatch(data: ServiceQuoteBatch): any {
  }

  formatQuoteItem(data: ServiceQuote): any {
  }

  /*formatHistorical(historical: ServiceHistorical): any {
    const output = [];

    historical.forEach((symbolData) => {
      // const m = moment(symbolData.date);
      // console.log(`${symbolData.symbol}, ${m.format('yyyy-MM-DD')}, ${symbolData.close},`);
      if (!(symbolData.symbol.substr(0, 1) === '^')) {
        // Indexes don't have a number of stocks
        output.push(',');
      }
      output.push(`${symbolData.close},`);
      if (!(symbolData.symbol.substr(0, 1) === '^')) {
        // Indexes don't have a number of stocks
        output.push(',');
      }
    });
    return output;
  }

  formatHistoricalBatch(historicalBatch: ServiceHistoricalBatch): any {
    // const output = [];
    //
    // const symbols = Array.from(historicalBatch.keys());
    // // console.log(`Date, ${symbols.join(',')}`);
    // output.push(`Date, ${symbols.join(',')}`);
    const symbols = Array.from(historicalBatch.keys()); // Array.from(quoteBatchMap.keys());
    const output = this.buildSymbolsHeader(symbols);
    // const symbols = Array.from(quoteBatchMap.keys());
    if (!(symbols && symbols.length)) {
      return;
    }

    // Build data structure to assist
    const dateMap = this.buildDataMap(historicalBatch);

    dateMap.forEach((historical, dateString) => {
      // console.log(`\n${dateString},`);
      output.push(`${dateString},`);
      historical.forEach((symbolData) => {
        // The first cell is for the # shares which is in the spreadsheet.  Use 'additive' paste
        // output.push(',' + symbolData[0].close + ',');
        output.push(...this.formatHistorical(symbolData));
      });
      output.push('\n');
    });
    return output.join('');
  }

  private buildDataMap(
    historicalBatch: ServiceHistoricalBatch,
  ): Map<string, ServiceHistoricalBatch> {
    const dateMap = new Map<string, ServiceHistoricalBatch>();
    // let symbolMap;
    historicalBatch.forEach((historical, symbol) => {
      // console.log(`\n,,`);
      // output += `\n,,`;
      // symbolMap = symbolMap.get(symbol) ||
      historical.forEach((symbolData) => {
        const mF = moment(symbolData.date).format('YYYY-MM-DD');
        if (!dateMap.has(mF)) {
          const symbolMap = new Map<string, ServiceHistorical>();
          dateMap.set(mF, symbolMap);
        }
        const symbolMap = dateMap.get(mF);
        symbolMap.set(symbol, [symbolData]);
      });
    });
    return dateMap;
  }

  /!** Quote - Price *********************************************!/

  formatQuote(data: ServiceQuote | ServiceQuoteBatch): any {
    // if (this.keyAsModule(data as ServiceQuote)) {
    if (data instanceof Map) {
      return this.formatQuoteBatch(data as ServiceQuoteBatch);
    } else {
      return this.formatQuoteItem(data as ServiceQuote);
    }
  }

  // Assuming just dealing with the price module at the moment
  formatQuoteItem(data: ServiceQuote): any {
    const output = [];
    // The extra col before the price (except index) is the # of shares and is managed in the spreadsheet
    if (!(data.price.symbol.substr(0, 1) === '^')) {
      // Indexes don't have a number of stocks
      output.push(',');
    }
    if (data?.price?.regularMarketPrice != null) {
      if (typeof data.price.regularMarketPrice === 'object') {
        // some weird thing Yahoo Finance does occasionally
        const val = data?.price?.regularMarketPrice
          ? data?.price?.regularMarketPrice?.['raw']
          : 0;
        output.push(`${val},`);
      } else {
        output.push(`${data?.price?.regularMarketPrice},`);
      }
    }
    // The extra col after the price (except index) is the $ spent on shares and is managed in the spreadsheet
    if (!(data.price.symbol.substr(0, 1) === '^')) {
      // Indexes don't have a number of stocks
      output.push(',');
    }
    return output.join('');
  }

  buildSymbolsHeader(symbols: string[]): string[] {
    if (!(symbols && symbols.length)) {
      return [];
    }

    const output = ['date', ','];

    symbols.forEach((s) => {
      // The extra col before the price (except index) is the # of shares and is managed in the spreadsheet
      if (!(s.substr(0, 1) === '^')) {
        output.push(',');
      }
      output.push(s);
      output.push(',');
      // The extra col after the price (except index) is the $ spent on shares and is managed in the spreadsheet
      if (!(s.substr(0, 1) === '^')) {
        output.push(',');
      }
    });
    output.push(`\n`);
    return output;
  }

  formatQuoteBatch(quoteBatchMap: ServiceQuoteBatch): any {
    const symbols = Array.from(quoteBatchMap.keys());
    const output = this.buildSymbolsHeader(symbols);
    // const symbols = Array.from(quoteBatchMap.keys());
    if (!(symbols && symbols.length)) {
      return;
    }

    // symbols.forEach(s => {
    //   output.push(',')
    //   if (!(s.substr(0, 1) === '^')) {
    //     // Indexes don't have a number of stocks
    //     output.push(',')
    //   }
    // output.push(s);
    // });
    // output.push(`\n`);
    // return output.join('');

    // TODO - allow for if have mixed stock exchanges
    // Assume the market closing date / time of every item is the same (that is, assume same stock exchange)
    const marketCloseField = (quoteBatchMap.get(symbols[0]) as ServiceQuote)
      .price.regularMarketTime;
    const marketCloseDate = moment(marketCloseField); // this assumes same X (same date)
    output.push(`${marketCloseDate.format('YYYY-MM-DD')},`);

    symbols.forEach((s) => {
      output.push(this.formatQuoteItem(quoteBatchMap.get(s) as ServiceQuote));
    });

    output.push('\n');
    return output.join('');
  }*/

}
