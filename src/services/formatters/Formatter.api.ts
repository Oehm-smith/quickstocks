import { ServiceHistorical, ServiceHistoricalBatch } from '../serviceProviders/ServiceProviderHistory.model';
import { ServiceQuote, ServiceQuoteBatch } from '../serviceProviders/ServiceProviderQuote.model';
import { Period } from '../serviceProviders/serviceProvider.API';

export abstract class FormatterApi {
  private outFormatterType: string;
  protected period: Period;

  constructor(outFrmatter: string, period: Period) {
    this.outFormatterType = outFrmatter;
    this.period = period;
    // console.log(`FormatterApi - outFormatterType: ${outFrmatter}`);
  }

  abstract format(data: ServiceHistoricalBatch): any;

  abstract formatHistorical(historical: ServiceHistorical): any;

  abstract formatHistoricalBatch(historicalBatch: ServiceHistoricalBatch): any;

  abstract formatQuote(data: ServiceQuote | ServiceQuoteBatch): any;

  abstract formatQuoteItem(data: ServiceQuote): any;

  abstract formatQuoteBatch(data: ServiceQuoteBatch): any;

  /**
   * To be returned in the response.
   */
  abstract getContentType(): string;

  /**
   * To be returned in the response.  Optional - return null or '' or undefined if not used.
   */
  abstract getContentDisposition(): string;

  public isIndexPattern = /^\^.*/;

  public isIndexFund(symbol: string): boolean {
    return this.isIndexPattern.test(symbol);
  }
}
