import { FormatterApi } from './Formatter.api';
import { ServiceHistorical, ServiceHistoricalBatch } from '../serviceProviders/ServiceProviderHistory.model';
import { ServiceQuote, ServiceQuoteBatch } from '../serviceProviders/ServiceProviderQuote.model';

const newline = '\n';
const singleAfter = ',';
const doubleAfter = ',,';
const tripleAfter = ',,,';

export class JSONFormatter extends FormatterApi {
  getContentType(): string {
    return 'application/json';
  }

  /**
   * To be returned in the response.  Optional - return null or '' or undefined if not used.
   */
  getContentDisposition(): string {
    return undefined;
  }

  format(data: ServiceHistoricalBatch): string {
    return JSON.stringify(Object.fromEntries(data));
  }

  private formatHeader(data: ServiceHistoricalBatch) {
    let symbols;

    // if (Array.isArray(data)) {
    //   symbols = [data[0].symbol];
    // } else {
    symbols = (data as ServiceHistoricalBatch).keys();
    // }
    const output: string[] = [];

    output.push('date');
    output.push(doubleAfter);
    symbols.forEach(s => {
      output.push(s);
      if (this.isIndexFund(s)) {
        output.push(doubleAfter);
      } else {
        output.push(tripleAfter);
      }
    });
    return output;
  }

  // format(data: ServiceHistorical | ServiceHistoricalBatch): any {
  formatHistory(data: ServiceHistoricalBatch): any {
    // return {output: 'formatted output: json'};
    const output: string[] = [];
    const handleOne = (dataArray: any) => {
      output.push(...dataArray);
      output.push(newline);
    };


    handleOne(this.formatHeader(data));

    // if (Array.isArray(data)) {
    //   // Just one symbol
    //   handleOne(this.formatHistorical(data as ServiceHistorical));
    // } else {
    // const aggregateByDate: ServiceHistorical[] =
    const mapData = data as Map<string, ServiceHistorical>;
    const symbols = (mapData).keys();
    for (const s in symbols) {
      handleOne(this.formatHistorical(mapData.get(s)));
    }
    // return this.formatHistoricalBatch(data as ServiceHistoricalBatch);
    // }
    return output;
  }

  /**
   * @invariant - the array of ServiceHistoryDataPoint only has the
   * @param historical - array of ServiceHistoryDataPoint
   */
  formatHistorical(historical: ServiceHistorical): string {
    const dps = ['    '];

    // historical.forEach(h => {
    //   dps.push(`date: ${h.date}, close: ${h.close}, symbol: ${h.symbol}`);
    // });
    // return `[DataPoints - ${dps.join('\n')}]\n`
    // return historical;
    const output: string[] = [];
    const symbol = 'cba.zxya'; //historical.symbol
    output.push(symbol);
    output.push(singleAfter);
    if (this.isIndexFund(symbol)) {
      output.push(doubleAfter);
    } else {
      output.push(tripleAfter);
    }
    return output.join(',');
  }

  formatHistoricalBatch(historicalBatch: ServiceHistoricalBatch): object {
    const obj = {};
    historicalBatch.forEach((value, key) => {
      obj[key] = this.formatHistorical(value);
    });
    return obj;
    // return `[YahooFinance HistoricalBatch - \n` +
    //   `    ${this.JSONMapString(historicalBatch)}\n` +
    //   `]`;
  }

  formatQuote(data: ServiceQuote | Map<string, ServiceQuote>): any {
    if (Array.isArray(data)) {
      return this.formatQuoteItem(data as ServiceQuote);
    } else {
      return this.formatQuoteBatch(data as ServiceQuoteBatch);
    }
  }

  formatQuoteBatch(data: Map<string, ServiceQuote>): any {
    return this.strMapToObj(data);
  }

  /**
   * From https://2ality.com/2015/08/es6-map-json.html.
   * @param strMap
   */
  strMapToObj = (data: Map<string, ServiceQuote>) => {
    const obj = Object.create(null);
    for (const [k, v] of data) {
      // We don’t escape the key '__proto__'
      // which can cause problems on older engines
      obj[k] = v;
    }
    return obj;
  };

  formatQuoteItem(data: ServiceQuote): any {
    console.log(`${JSON.stringify(data, null, 2)}`);
    return data;
  }

}
