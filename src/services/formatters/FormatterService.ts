import { ServiceHistoricalBatch } from '../serviceProviders/ServiceProviderHistory.model';
import { OutFormat } from '../../controllers/QueryTypes';
import { FormatterFactory } from './FormatterFactory';
import { FormatterApi } from './Formatter.api';
import { Period } from '../serviceProviders/serviceProvider.API';

export class FormatterService {
  private outFormat: OutFormat;
  private period: Period;
  private formatter: FormatterApi;
  private formattedOutput: string;

  constructor(format: OutFormat, period: Period) {
    this.outFormat = format;
    this.period = period
  }

  format(serviceHistory: ServiceHistoricalBatch): string {
    this.formatter = FormatterFactory.formatter(this.outFormat, this.period);
    const formattedOutput = this.formatter.format(serviceHistory);
    return formattedOutput;
  }

  getContentType(): string {
    return this.formatter.getContentType();
  }

  getContentDisposition(): string {
    return this.formatter.getContentDisposition();
  }
}