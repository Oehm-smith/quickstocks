/**
 * Common model used for Historical data.  Same as YahooFinance.  Thus why prepended with 'Common'
 * 24.12.24 - but using YahooFinance2 and its' different.  However will still use this as intermediate format.
 */
export interface ServiceHistoryDataPoint {
  date: Date; // eg. Nov 07 2013 00:00:00 GMT-0500 (EST),
  open: number; // eg. 45.1,
  high: number; // eg. 50.09,
  low: number; // eg. 44,
  close: number; // eg. 44.9,
  volume: number; // eg. 117701700,
  adjClose: number; // eg. 44.9,
  symbol: string; // 'TWTR'
}

export type ServiceHistorical = ServiceHistoryDataPoint[];

// Map of Symbol to CommonHistorical (CommonDataPoint[])
export type ServiceHistoricalBatch = Map<string, ServiceHistorical>;
export type ServiceHistoricalBatchByDate = Map<number, ServiceHistorical>;
