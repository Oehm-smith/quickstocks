export interface ServiceQuoteSummaryDetail {
  // summaryDetail: {
  maxAge: number;
  priceHint: number;
  previousClose: number;
  open: number;
  dayLow: number;
  dayHigh: number;
  regularMarketPreviousClose: number;
  regularMarketOpen: number;
  regularMarketDayLow: number;
  regularMarketDayHigh: number;
  dividendRate: number;
  dividendYield: number;
  exDividendDate: Date;
  payoutRatio: number;
  fiveYearAvgDividendYield: number;
  beta: number;
  trailingPE: number;
  forwardPE: number;
  volume: number;
  regularMarketVolume: number;
  averageVolume: number;
  averageVolume10days: number;
  averageDailyVolume10Day: number;
  bid: number;
  ask: number;
  bidSize: number;
  askSize: number;
  marketCap: number;
  fiftyTwoWeekLow: number;
  fiftyTwoWeekHigh: number;
  priceToSalesTrailing12Months: number;
  fiftyDayAverage: number;
  twoHundredDayAverage: number;
  trailingAnnualDividendRate: number;
  trailingAnnualDividendYield: number;
  currency: string;
  fromCurrency: string;
  toCurrency: string;
  lastMarket: string;
  algorithm: string;
  tradeable: false;
  // }
}

export interface ServiceQuotePrice {
  //    price: {
  maxAge: number;
  regularMarketChangePercent: number;
  regularMarketChange: number;
  regularMarketTime: Date;
  priceHint: number;
  regularMarketPrice: number;
  regularMarketDayHigh: number;
  regularMarketDayLow: number;
  regularMarketVolume: number;
  averageDailyVolume10Day: number;
  averageDailyVolume3Month: number;
  regularMarketPreviousClose: number;
  regularMarketSource: string;
  regularMarketOpen: number;
  exchange: string;
  exchangeName: string;
  exchangeDataDelayedBy: number;
  marketState: string;
  quoteType: string;
  symbol: string;
  underlyingSymbol: string;
  shortName: string;
  longName: string;
  currency: string;
  currencySymbol: string;
  fromCurrency: string;
  toCurrency: string;
  lastMarket: string;
  marketCap: number;
  // }
}

export interface ServiceQuote {
  summaryDetail?: ServiceQuoteSummaryDetail;
  price?: ServiceQuotePrice;
}

export type ServiceQuoteBatch = Map<string, ServiceQuote>;

/**
 * The different modules that quote can return where each module is a different set of data and is reflected in
 * the interfaces above (prefixed with 'ServiceQuote' eg 'ServiceQuotePrice'.  But only implementing them when required.
 * See https://github.com/pilwon/node-yahoo-finance/blob/HEAD/docs/quote.md
 */
export enum QuoteModules {
  summaryDetail = 'summaryDetail',
  price = 'price',
  recommendationTrend = 'recommendationTrend',
  earnings = 'earnings',
  calendarEvents = 'calendarEvents',
  upgradeDowngradeHistory = 'upgradeDowngradeHistory',
  defaultKeyStatistics = 'defaultKeyStatistics',
  summaryProfile = 'summaryProfile',
  financialData = 'financialData',
}
