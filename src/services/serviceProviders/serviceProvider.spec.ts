import * as sinon from 'sinon';

import { ServiceProvider } from './serviceProvider';
import { Period, string2Period } from './serviceProvider.API';
import { TestHelper } from '../../../test/testData/testHelper';
import { Test } from '@nestjs/testing';
import { QuoteModules, ServiceQuote } from './ServiceProviderQuote.model';
import { ServiceHistorical, ServiceHistoricalBatch } from './ServiceProviderHistory.model';
import { afterEach, beforeEach, describe, expect, it, xdescribe } from '@jest/globals';
import { YahooFinance2 } from './YahooFinance2/YahooFinance2';
import { YahooFinance2ServiceProvider } from './YahooFinance2/YahooFinance2.serviceProvider';

interface SuccinctDate {
  year: number;
  month: number;
  day: number;
}

const buildSuccinctDate = (dates: string | string[]): SuccinctDate[] => {
  if (Array.isArray(dates)) {
    const result = [];
    dates.forEach((d) => result.push(...buildSuccinctDate(d)));
    return result;
  } else {
    const dateParts = dates.split('-'); // Dates are like '2020-04-21'
    return [
      {
        year: Number.parseInt(dateParts[0]),
        month: Number.parseInt(dateParts[1]) - 1,
        day: Number.parseInt(dateParts[2]),
      },
    ];
  }
};

const checkSymbolDateMapOneSymbol = (
  result: ServiceHistoricalBatch,
  symbol: string,
  expectdDates: SuccinctDate[],
) => {
  //'cba.ax', [[2020, 4, 11]]);
  console.debug(`Expect symbol to be defined: ${symbol}`);
  expect(result.get(symbol)).toBeDefined();
  expect(Array.isArray(result.get(symbol))).toBeTruthy();
  expect(result.get(symbol)).toHaveLength(expectdDates.length);
  expectdDates.forEach((expectedDate, i) => {
    expect(result.get(symbol)[i].date instanceof Date).toBeTruthy();
    expect(result.get(symbol)[i].date.getFullYear()).toEqual(expectedDate.year);
    expect(result.get(symbol)[i].date.getMonth()).toEqual(expectedDate.month);
    expect(result.get(symbol)[i].date.getDate()).toEqual(expectedDate.day);
    expect(result.get(symbol)[i].symbol).toEqual(symbol);
  });
};

const checkSymbolDateMap = (
  result: ServiceHistoricalBatch,
  symbols: string[],
  expectdDates: SuccinctDate[],
) => {
  //'cba.ax', [[2020, 4, 11]]);
  console.log(
    `  checkSymbolDateMap - symbols: ${JSON.stringify(symbols)}, dates: ${JSON.stringify(expectdDates)}`,
  );
  symbols.forEach((symbol) => {
    checkSymbolDateMapOneSymbol(result, symbol, expectdDates);
  });
};

/**
 * This is a helper function to enable us to use the same testing code.  It builds an artificial CommonHistoricalBatch - ie. Map with symbol as key
 * @param symbol
 * @param result
 */
const buildResultMap = (symbol, result) => {
  const resultMap = new Map<string, ServiceHistorical>();
  resultMap.set(symbol, result);
  return resultMap;
};

xdescribe('ServiceProvider', () => {
  describe(`historical`, () => {
    let serviceProvider;
    let YahooFinance2ServiceProvider;
    let yahooFinanceStub;
    let yahooFinance;
    // beforeEach(async () => {
    // const moduleRef = await Test.createTestingModule({
    //   providers: [ServiceProvider, AlphaVantage, AlphaVantageServiceProvider],
    // }).compile();
    //
    // serviceProvider = moduleRef.get<ServiceProvider>(ServiceProvider);
    // });

    describe(`lookup single`, () => {
      beforeEach(async () => {
        yahooFinance = new YahooFinance2();
        yahooFinanceStub = sinon
          .stub(yahooFinance, 'historical')
          .callsFake(
            (
              symbol: string,
              startDate: string,
              endDate: string,
            ): Promise<ServiceHistorical> => {
              const file =
                './test/testData/YahooFinance/cba_2020-05-11_to_2020-05-12_ServiceHistorical.json';
              return TestHelper.getAndPrepareTestDataServiceHistorical(
                file,
                startDate,
                endDate,
              ) as Promise<ServiceHistorical>;
            },
          );
        YahooFinance2ServiceProvider = new YahooFinance2ServiceProvider(
          yahooFinance,
        );
        serviceProvider = new ServiceProvider(YahooFinance2ServiceProvider);
      });

      afterEach(async () => {
        yahooFinanceStub.restore();
      });

      it(`search for CBA Tue 11.5.20`, async () => {
        const symbol = 'cba.ax';
        const from = '2020-05-11';
        const result = (await serviceProvider.history(
          symbol,
          from,
          from,
        )) as ServiceHistorical;

        const resultMap = buildResultMap(symbol, result);
        checkSymbolDateMapOneSymbol(resultMap, symbol, buildSuccinctDate(from));
      });

      it(`search for CBA Wed 2020-05-11 and next day`, async () => {
        const symbol = 'cba.ax';
        const from = '2020-05-11';
        const to = '2020-05-12';

        const result = (await serviceProvider.history(
          'cba.ax',
          from,
          to,
        )) as ServiceHistorical;
        const resultMap = buildResultMap(symbol, result);
        checkSymbolDateMapOneSymbol(
          resultMap,
          symbol,
          buildSuccinctDate([to, from]),
        );
      });

      it(`should fail if no symbol`, async () => {
        const from = '2020-05-13';
        return expect(
          serviceProvider.history('', from, from),
        ).rejects.toThrowError(`Missing symbol`);
      });
    });

    describe(`lookup batch`, () => {
      beforeEach(async () => {
        yahooFinance = new YahooFinance2();
        yahooFinanceStub = sinon
          .stub(yahooFinance, 'historicalBatch')
          .callsFake(
            (
              symbol: string,
              startDate: string,
              endDate: string,
            ): Promise<ServiceHistoricalBatch> => {
              const file =
                './test/testData/YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json';
              return TestHelper.getAndPrepareTestDataServiceHistorical(
                file,
                startDate,
                endDate,
              ) as Promise<ServiceHistoricalBatch>;
            },
          );
        YahooFinance2ServiceProvider = new YahooFinance2ServiceProvider(
          yahooFinance,
        );
        serviceProvider = new ServiceProvider(YahooFinance2ServiceProvider);
      });

      afterEach(async () => {
        yahooFinanceStub.restore();
      });

      it(`search for CBA, BHP Tue 11.5.20`, async () => {
        const from = '2020-05-11';
        const result = (await serviceProvider.history(
          ['cba.ax', 'bhp.ax'],
          from,
          from,
        )) as ServiceHistoricalBatch;
        expect(result).toBeDefined();
        expect(result instanceof Map).toBeTruthy();

        checkSymbolDateMap(
          result,
          ['cba.ax', 'bhp.ax'],
          [{ year: 2020, month: 4, day: 11 }],
        );
      });

      it(`search for CBA, BHP Wed 2020-05-11 and next day`, async () => {
        const from = '2020-05-11';
        const to = '2020-05-12';
        const result = (await serviceProvider.history(
          ['cba.ax', 'bhp.ax'],
          from,
          to,
        )) as ServiceHistoricalBatch;
        expect(result).toBeDefined();
        expect(result instanceof Map).toBeTruthy();

        checkSymbolDateMap(
          result,
          ['cba.ax', 'bhp.ax'],
          [
            { year: 2020, month: 4, day: 12 },
            {
              year: 2020,
              month: 4,
              day: 11,
            },
          ],
        );
      });

      it(`should fail if no symbol`, async () => {
        const from = '2020-05-13';
        return expect(
          serviceProvider.history([''], from, from),
        ).rejects.toThrowError(`Missing symbol`);
      });
    });

    describe('string2Period', () => {
      it('undefined', () => {
        expect(string2Period(undefined)).toEqual(undefined);
      });
      it('daily', () => {
        expect(string2Period('d')).toEqual(Period.d);
        expect(string2Period('daily')).toEqual(Period.d);
        expect(string2Period('DAILY')).toEqual(Period.d);
      });
      it('weekly', () => {
        expect(string2Period('w')).toEqual(Period.w);
        expect(string2Period('weekly')).toEqual(Period.w);
        expect(string2Period('WEEKLY')).toEqual(Period.w);
      });
      it('monthly', () => {
        expect(string2Period('m')).toEqual(Period.m);
        expect(string2Period('monthly')).toEqual(Period.m);
        expect(string2Period('MONTHLY')).toEqual(Period.m);
      });
      it('v', () => {
        expect(string2Period('v')).toEqual(Period.v);
        expect(string2Period('V')).toEqual(Period.v);
        expect(string2Period('dividends')).toEqual(Period.v);
      });
    });
  });

  describe(`quote`, () => {
    let serviceProvider;

    // I'm not going to wet my pants over trying to abstract the network away from this test like I did with 'historical' above
    // Since the YahooFinance2ServiceProvider pretty much just passes through the result from YahooFinance which is from Finance.Yahoo
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [ServiceProvider, YahooFinance2ServiceProvider, YahooFinance2],
      }).compile();

      serviceProvider = moduleRef.get<ServiceProvider>(ServiceProvider);
    });
    describe(`single symbol`, () => {
      it(`Get CBA current price`, async () => {
        const result = (await serviceProvider.quote('cba.ax', [
          QuoteModules.price,
        ])) as ServiceQuote;
        expect(result).toBeDefined();
        const price = result.price;
        TestHelper.checkPrice(price, 'cba.ax');
      });
    });

    describe(`batch symbols`, () => {
      it(`Get CBA, BHP current price`, async () => {
        const result = await serviceProvider.quote(
          ['cba.ax', 'bhp.ax'],
          [QuoteModules.price],
        );
        expect(result).toBeDefined();
        const cba = result.get('cba.ax');
        expect(cba).toBeDefined();
        let price = cba.price;
        TestHelper.checkPrice(price, 'cba.ax');

        const bhp = result.get('bhp.ax');
        expect(bhp).toBeDefined();
        price = bhp.price;
        TestHelper.checkPrice(price, 'bhp.ax');
      });
    });
  });
});
