/**
 * Define API for providers.  This is a generic interface used "internally" in this application.
 *
 */

import { ServiceHistoricalBatch } from './ServiceProviderHistory.model';
import { QuoteModules, ServiceQuote, ServiceQuoteBatch } from './ServiceProviderQuote.model';

export enum Period {
  'd' = 'd', // daily',
  'w' = 'w', // weekly',
  'm' = 'm', // monthly',
  'v' = 'v', //dividends'
}

export enum Period2 {
  'd' = '1d', // daily',
  'w' = '1w', // weekly',
  'm' = '1m', // monthly',
  'v' = 'v', //dividends'
}

export const string2Period = (periodString: string): Period => {
  if (!periodString) {
    return undefined;
  }
  if (periodString.toLocaleLowerCase().match(/dividend/)) {
    return Period.v;
  }
  switch (periodString.toLocaleLowerCase().split('')[0]) {
    case 'd':
      return Period.d;
      break;
    case 'w':
      return Period.w;
      break;
    case 'm':
      return Period.m;
      break;
    case 'v':
      return Period.v;
      break;
    default:
      throw new Error(`Can't match text to Period: ${periodString}`);
  }
};

export const ServiceProviderAPI_TOKEN = Symbol('ServiceProviderAPI_TOKEN');

export interface ServiceProviderAPI {
  /**
   * Lookup historical share value(s)
   * @param symbols - single stock symbol or array of symbols / stocks to lookup
   * @param startDate - date to start at
   * @param endDate - date (inclusive) to end at
   * @param period (Optional - default is 'd') - 'd'ay, 'w'eek, 'm'onth or 'v' for dividends only
   * @return array of symbol values if a string symbol, or a Map keyed by symbol if string[] symbols used
   *
   * If startDate is undefined then start from first possible historical data
   * If endDate is undefined then finish with most recent data
   * Period provides the freqency of dataPoints within the date range
   */
  history(
    symbols: string | string[],
    startDate: string,
    endDate?: string,
    period?: Period,
  ): Promise<ServiceHistoricalBatch>;

  /**
   * Return the current (realtime-ish) data described by the given module for the given symbol.
   * See https://github.com/pilwon/node-yahoo-finance/blob/HEAD/docs/quote.md.
   *
   * @param symbols
   * @param modules what data to get - defaults to 'price' in implementation
   */
  quote(
    symbols: string | string[],
    modules: QuoteModules[],
  ): Promise<ServiceQuote | ServiceQuoteBatch>;
}
