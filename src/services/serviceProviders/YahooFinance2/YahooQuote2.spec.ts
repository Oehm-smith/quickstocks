import { quote } from 'yahoo-finance';
import { it, xdescribe } from '@jest/globals';

/**
 * This was used to work out how to use the YahooFinance Quote API endpoint, and then the data format
 * (See ServiceProviderQuote.model and YahooQuote.model).
 */
xdescribe('yahoo quote', () => {
  it('cba today', async () => {
    let quoteResult;
    try {
      quoteResult = await quote({
        symbol: 'cba.ax',
        modules: ['price', 'summaryDetail'], // optional; default modules.
      });
    } catch (err) {
      if (err) {
        console.log(`err: ${JSON.stringify(err)}`);
        return;
      }
    }
    console.log(`quote: ${JSON.stringify(quoteResult, null, 2)}`);
  });
  it('cba, bhp today', async () => {
    let quoteResult;
    try {
      quoteResult = await quote({
        symbols: ['cba.ax', 'bhp.ax'],
        modules: ['price', 'summaryDetail'], // optional; default modules.
      });
    } catch (err) {
      if (err) {
        console.log(`err: ${JSON.stringify(err)}`);
        return;
      }
    }
    console.log(`quote: ${JSON.stringify(quoteResult, null, 2)}`);
  });
});
