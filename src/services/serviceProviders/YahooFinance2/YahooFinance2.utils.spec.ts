import { describe, expect, it, test } from '@jest/globals';
import { DataHelpers } from '../../../utils/TestUtils';
import { ServiceHistoricalBatch, ServiceHistoricalBatchByDate } from '../ServiceProviderHistory.model';
import { YahooFinance2Utils } from './YahooFinance2.utils';
import { DateUtils } from '../../../utils/DateUtils';
import { SortOrder } from './YahooHistory2.model';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

/*const printHex = (string: string): void => {
  console.log(string)
  const hexString = Array.from(string)
    .map(char => char.charCodeAt(0).toString(16).padStart(2, '0')) // Convert to hex
    .join(' '); // Separate each hex value with a space
  console.log(hexString);
}*/

describe('YahooFinance2 Utils', () => {
  // afterEach(() => {
  //   const currentTestName = expect.getState().currentTestName;
  //   let testStatus = true;
  //   testStatus = expect.getState().isExpectingAssertions || testStatus; // Adjust as needed
  //   console.log(`expect.getState().isExpectingAssertions: ${expect.getState().isExpectingAssertions}`)
  //
  //   if (! testStatus ) {
  //     // failedTests.push(currentTestName);
  //     console.log(`Test failed: ${currentTestName}`);
  //   } else {
  //     console.log(`Test passed: ${currentTestName}`);
  //   }
  //
  //   const state = expect.getState();
  //   console.log(`expect: ${JSON.stringify(expect, null, 2)}`)
  // });
  // afterAll(() => {
  // })

  describe('createServiceHistoryMap', () => {
    it('input ServiceHistorical', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_2020-05-11_to_2020-05-12_ServiceHistorical.json');
      const serviceHistoricalMap = YahooFinance2Utils.createServiceHistoryMap(data);
      const symbols = YahooFinance2Utils.getUniqueSymbols(serviceHistoricalMap);
      expect(serviceHistoricalMap instanceof Map).toBeTruthy();
      expect(symbols.length).toEqual(1);
      expect(symbols[0]).toEqual('CBA.AX');
      expect(serviceHistoricalMap.get('CBA.AX')).not.toEqual(undefined);
      expect(serviceHistoricalMap.get('BHP.AX')).toEqual(undefined);
      expect(serviceHistoricalMap.get('CBA.AX').length).toEqual(2);
    });
    it('input ServiceHistoricalBatch Map', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
      expect(data instanceof Map).toBeTruthy();
      const serviceHistoricalMap = YahooFinance2Utils.createServiceHistoryMap(data);
      const symbols = YahooFinance2Utils.getUniqueSymbols(serviceHistoricalMap);
      expect(serviceHistoricalMap instanceof Map).toBeTruthy();
      expect(symbols.length).toEqual(2);
      expect(symbols).toEqual(['CBA.AX', 'BHP.AX']);
      expect(serviceHistoricalMap.get('CBA.AX')).not.toEqual(undefined);
      expect(serviceHistoricalMap.get('BHP.AX')).not.toEqual(undefined);
      expect(serviceHistoricalMap.get('CBA.AX').length).toEqual(2);
      expect(serviceHistoricalMap.get('BHP.AX').length).toEqual(2);
    });
  });

  describe('getDataForSymbolAtDate', () => {
    it('single symbol - symbol does not exist', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_2020-05-11_to_2020-05-12_ServiceHistorical.json');
      const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
      const dates = YahooFinance2Utils.getDates(symbolMap);

      const falseSymbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('AINTEXIST.ax', dates[0], symbolMap);
      expect(falseSymbolDataForDate).toEqual(null);
    });
    it('single symbol - exists', () => {
      try {
        const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_2020-05-11_to_2020-05-12_ServiceHistorical.json');
        const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
        const dates = YahooFinance2Utils.getDates(symbolMap);

        const symbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('CBA.AX', dates[0], symbolMap);
        expect(symbolDataForDate).not.toEqual(null);
        expect(symbolDataForDate[0].symbol).toEqual('CBA.AX');
      } catch (err) {
        console.log('ERRROR: ', err); // Only shows message
        throw err;
      }

    });
    it('multi symbol - symbol does not exist', () => {
      try {
        const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
        const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
        const dates = YahooFinance2Utils.getDates(symbolMap);

        const falseSymbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('AINTEXIST.ax', dates[0], symbolMap);
        expect(falseSymbolDataForDate).toEqual(null);
      } catch (err) {
        console.log('ERRROR: ', err); // Only shows message
        throw err;
      }

    });

    it('multi symbol ', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
      const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
      const dates = YahooFinance2Utils.getDates(symbolMap);

      const falseSymbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('AINTEXIST.ax', dates[0], symbolMap);
      expect(falseSymbolDataForDate).toEqual(null);

      const symbolDataForDate1 = YahooFinance2Utils.getDataForSymbolAtDate('CBA.AX', dates[0], symbolMap);
      expect(symbolDataForDate1).not.toEqual(null);
      expect(symbolDataForDate1[0].symbol).toEqual('CBA.AX');

      const symbolDataForDate2 = YahooFinance2Utils.getDataForSymbolAtDate('BHP.AX', dates[0], symbolMap);
      expect(symbolDataForDate2).not.toEqual(null);
      expect(symbolDataForDate2[0].symbol).toEqual('BHP.AX');
    });
  });

  describe('aggregateByDate', () => {
    it('multi symbol - symbol does not exist', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
      const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
      const dates = YahooFinance2Utils.getDates(symbolMap);

      const falseSymbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('AINTEXIST.ax', dates[0], symbolMap);
      expect(falseSymbolDataForDate).toEqual(null);
    });

    it('multi symbol ', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
      const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
      const dates = YahooFinance2Utils.getDates(symbolMap);

      const falseSymbolDataForDate = YahooFinance2Utils.getDataForSymbolAtDate('AINTEXIST.ax', dates[0], symbolMap);
      expect(falseSymbolDataForDate).toEqual(null);

      const symbolDataForDate1 = YahooFinance2Utils.getDataForSymbolAtDate('CBA.AX', dates[0], symbolMap);
      expect(symbolDataForDate1).not.toEqual(null);
      expect(symbolDataForDate1[0].symbol).toEqual('CBA.AX');

      const symbolDataForDate2 = YahooFinance2Utils.getDataForSymbolAtDate('BHP.AX', dates[0], symbolMap);
      expect(symbolDataForDate2).not.toEqual(null);
      expect(symbolDataForDate2[0].symbol).toEqual('BHP.AX');
    });

    it('single symbol', () => {
      try {
        const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_2020-05-11_to_2020-05-12_ServiceHistorical.json');
        const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
        const serviceByDate = YahooFinance2Utils.aggregateByDate(symbolMap, SortOrder.descending);

        expect(DateUtils.areDateListsEqual(YahooFinance2Utils.getDates(symbolMap), [...serviceByDate.keys()], SortOrder.descending));

        const dateKeys = [...serviceByDate.keys()];
        expect(dateKeys.length).toEqual(2);
        expect(serviceByDate.get(dateKeys[0]).length).toEqual(1);
        expect(serviceByDate.get(dateKeys[1]).length).toEqual(1);
        const date1 = serviceByDate.get(dateKeys[0]);
        expect(date1.length).toEqual(1);
        const cba = date1[0];
        expect(cba.date).toEqual(DateUtils.convertToDate(dateKeys[0]));
        expect(cba.symbol).toEqual('CBA.AX');
      } catch (err) {
        console.log('ERRROR: ', err); // Only shows message
        throw err;
      }
    });
    it('multi symbol 2 ', () => {
      const data = DataHelpers.getFileServiceHistoryDataPoints('YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_ServiceHistoricalBatch.json');
      const symbolMap = YahooFinance2Utils.convertServiceHistoricalToBatch(data);
      const byDate = YahooFinance2Utils.aggregateByDate(symbolMap, SortOrder.descending);

      expect(DateUtils.areDateListsEqual(YahooFinance2Utils.getDates(symbolMap), [...byDate.keys()], SortOrder.descending));

      const dateKeys = [...byDate.keys()];
      expect(dateKeys.length).toEqual(2);
      expect(byDate.get(dateKeys[0]).length).toEqual(2);
      let symbols = byDate.get(dateKeys[0]).map(s => s.symbol);
      expect(symbols.length).toEqual(2);
      const expectedSymbols = ['cba.ax', 'bhp.ax'];
      symbols.every(s => expectedSymbols.includes(s));
      expect(byDate.get(dateKeys[1]).length).toEqual(2);
    });

    test('should aggregate data by date in descending order', () => {
      const mockData: ServiceHistoricalBatch = new Map([
        [
          '^AXJO',
          [
            {
              date: new Date('2023-01-01'),
              open: 10,
              high: 15,
              low: 8,
              close: 12,
              volume: 1000,
              adjClose: 12.5,
              symbol: '^AXJO',
            },
            {
              date: new Date('2023-01-02'),
              open: 13,
              high: 18,
              low: 10,
              close: 15,
              volume: 1200,
              adjClose: 15,
              symbol: '^AXJO',
            },
          ],
        ],
        [
          'CBA.AX',
          [
            {
              date: new Date('2023-01-01'),
              open: 50,
              high: 55,
              low: 48,
              close: 52,
              volume: 2000,
              adjClose: 52.5,
              symbol: 'CBA.AX',
            },
            {
              date: new Date('2023-01-02'),
              open: 55,
              high: 60,
              low: 50,
              close: 58,
              volume: 2500,
              adjClose: 58,
              symbol: 'CBA.AX',
            },
          ],
        ],
      ]);

      const result: ServiceHistoricalBatchByDate = YahooFinance2Utils.aggregateByDate(mockData, SortOrder.descending);
      const dates = Array.from(result.keys());

      expect(dates).toEqual([new Date('2023-01-02').getTime(), new Date('2023-01-01').getTime()]);
      expect(result.get(new Date('2023-01-02').getTime())?.length).toBe(2);
      expect(result.get(new Date('2023-01-01').getTime())?.length).toBe(2);
    });

    test('should aggregate data by date in ascending order', () => {
      const mockData: ServiceHistoricalBatch = new Map([
        [
          '^AXJO',
          [
            {
              date: new Date('2023-01-01'),
              open: 10,
              high: 15,
              low: 8,
              close: 12,
              volume: 1000,
              adjClose: 12.5,
              symbol: '^AXJO',
            },
            {
              date: new Date('2023-01-02'),
              open: 13,
              high: 18,
              low: 10,
              close: 15,
              volume: 1200,
              adjClose: 15,
              symbol: '^AXJO',
            },
          ],
        ],
        [
          'CBA.AX',
          [
            {
              date: new Date('2023-01-01'),
              open: 50,
              high: 55,
              low: 48,
              close: 52,
              volume: 2000,
              adjClose: 52.5,
              symbol: 'CBA.AX',
            },
            {
              date: new Date('2023-01-02'),
              open: 55,
              high: 60,
              low: 50,
              close: 58,
              volume: 2500,
              adjClose: 58,
              symbol: 'CBA.AX',
            },
          ],
        ],
      ]);

      const result: ServiceHistoricalBatchByDate = YahooFinance2Utils.aggregateByDate(mockData, SortOrder.ascending);
      const dates = Array.from(result.keys());

      expect(dates).toEqual([new Date('2023-01-01').getTime(), new Date('2023-01-02').getTime()]);
      expect(result.get(new Date('2023-01-02').getTime())?.length).toBe(2);
      expect(result.get(new Date('2023-01-01').getTime())?.length).toBe(2);
    });

    test('should handle empty input gracefully', () => {
      const mockData: ServiceHistoricalBatch = new Map();

      const result: ServiceHistoricalBatchByDate = YahooFinance2Utils.aggregateByDate(mockData, SortOrder.descending);
      expect(result.size).toBe(0);
    });
  });

});