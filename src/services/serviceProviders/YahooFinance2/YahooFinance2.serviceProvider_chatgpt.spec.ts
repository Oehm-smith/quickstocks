import { Test, TestingModule } from '@nestjs/testing';
import { YahooFinance2ServiceProvider } from './YahooFinance2.serviceProvider';
import { YahooFinance2 } from './YahooFinance2';
import moment from 'moment';
import { afterEach, beforeEach, describe, expect, it } from '@jest/globals';
import { ServiceHistoricalBatch } from '../ServiceProviderHistory.model';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

// Mock implementation of YahooFinance2
const mockYahooFinance2 = {
  historical: jest.fn(),
  historicalBatch: jest.fn(),
};

describe('YahooFinance2ServiceProvider', () => {
  let service: YahooFinance2ServiceProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        YahooFinance2ServiceProvider,
        { provide: YahooFinance2, useValue: mockYahooFinance2 },
      ],
    }).compile();

    service = module.get<YahooFinance2ServiceProvider>(YahooFinance2ServiceProvider);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should format dates and fetch historical data for a single symbol', async () => {
    try {
      const symbol = 'AAPL';
      const from = '2023-12-01';
      const to = '2023-12-10';
      const mockResponse = new Map([
        [symbol, { meta: {}, quotes: [] }],
      ]); // Mocked MapChartResultsArray

      mockYahooFinance2.historical.mockResolvedValue(mockResponse);

      const result = await service.history(symbol, from, to);

      expect(mockYahooFinance2.historical).toHaveBeenCalledWith(
        [symbol],
        moment(from).format('YYYY-MM-DD'),
        moment(to).format('YYYY-MM-DD'),
        'd',
      );
      expect(result).toBeDefined();
    } catch (err) {
      console.log('ERRROR: ', err); // Only shows message
      throw err;
    }

  });

  it('should throw an error if single symbol is empty', async () => {
    await expect(service.history('', '2023-12-01', '2023-12-10')).rejects.toThrow('Missing symbol');
  });

  it('should format dates and fetch historical batch data for multiple symbols', async () => {
    try {
      const symbols = ['AAPL', 'MSFT'];
      const from = '2023-12-01';
      const to = '2023-12-10';
      const mockResponse = new Map([
        ['AAPL', { meta: {}, quotes: [] }],
        ['MSFT', { meta: {}, quotes: [] }],
      ]); // Mocked ChartResultArray Map

      mockYahooFinance2.historical.mockResolvedValue(mockResponse);

      const result = await service.history(symbols, from, to);

      expect(mockYahooFinance2.historical).toHaveBeenCalledWith(
        symbols,
        moment(from).format('YYYY-MM-DD'),
        moment(to).format('YYYY-MM-DD'),
        'd',
      );
      expect(result).toBeDefined();
      expect(result instanceof Map).toBe(true);
      expect((result as ServiceHistoricalBatch).size).toEqual(2);
    } catch (err) {
      console.log('ERRROR: ', err); // Only shows message
      throw err;
    }

  });

  it('should throw an error if batch symbols array is empty', async () => {
    await expect(service.history([], '2023-12-01', '2023-12-10')).rejects.toThrow('Missing symbol');
  });

  it('should handle undefined `to` date correctly', async () => {
    try {
      const symbol = 'AAPL';
      const from = '2023-12-01';
      const mockResponse = new Map([
        ['AAPL', { meta: {}, quotes: [] }],
      ]); // Mocked ChartResultArray Map
      mockYahooFinance2.historical.mockResolvedValue(mockResponse);

      const result = await service.history(symbol, from, undefined);

      expect(mockYahooFinance2.historical).toHaveBeenCalledWith(
        [symbol],
        moment(from).format('YYYY-MM-DD'),
        undefined,
        'd',
      );
      expect(result).toBeDefined();
    } catch (err) {
      console.log('ERRROR: ', err); // Only shows message
      throw err;
    }

  });

  it('should handle errors from YahooFinance2 for single symbol', async () => {
    const symbol = 'AAPL';
    const from = '2023-12-01';
    const to = '2023-12-10';

    mockYahooFinance2.historical.mockRejectedValue(new Error('API error'));

    await expect(service.history(symbol, from, to)).rejects.toThrow('API error');
  });

  it('should handle errors from YahooFinance2 for batch symbols', async () => {
    const symbols = ['AAPL', 'MSFT'];
    const from = '2023-12-01';
    const to = '2023-12-10';

    mockYahooFinance2.historical.mockRejectedValue(new Error('API error'));

    await expect(service.history(symbols, from, to)).rejects.toThrow('API error');
  });

  it('should correctly convert ChartResultArray to ServiceHistorical', async () => {
    const symbol = 'AAPL';
    const from = '2023-12-01';
    const to = '2023-12-10';
    const mockResponse = new Map([
      ['AAPL', { meta: {}, quotes: [{ date: '2020-05-12T04:00:00.000Z', open: 100 }] }],
    ]); // Mocked ChartResultArray Map

    mockYahooFinance2.historical.mockResolvedValue(mockResponse);

    const result = await service.history(symbol, from, to);

    expect(result).toBeDefined();
    // Add assertions to verify the conversion logic
  });
});
