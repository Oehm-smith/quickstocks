import yahooFinance from 'yahoo-finance2';

/**
 * This uses the Yahoo service (via NPM Library) in to the YahooFinance.model
 */
import { Period } from '../serviceProvider.API';
import { MapChartResultsArray } from './YahooHistory2.model';
import { QuoteModules } from '../ServiceProviderQuote.model';
import { YahooQuote, YahooQuoteBatch } from './YahooQuote2.model';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import { YahooFinance2Api } from './YahooFinance2.api';

/*const mapPeriod = (p: Period): string => {
  switch (p) {
    case Period.m:
      return '1m';
      break;
    case Period.w:
      return '1w';
      break;
    case Period.d:
    default:
      return '1d';
      break;
  }
};*/

export class YahooFinance2 implements YahooFinance2Api {
  // historical(symbol: string, from: string, to: string, period: Period = Period.d): Promise<Historical> {
  async historicalForSymbol(
    symbol: string,
    period1: string,
    period2: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    period: Period = Period.d,
  ): Promise<ChartResultArray> {
    // historical({ symbol, from, to, period }, (err, quotes) => {
    // TODO add interval
    // const interval = mapPeriod(period);

    // DEBUG
    // const query = 'amp.ax';
    // const queryOptions = { period1: '2021-05-08' /* ... */ };
    // const result2 = await yahooFinance.chart(query, queryOptions);
    // console.log(`random yahooFinance2/historical keys: ${JSON.stringify(Object.keys(result2))}`);
    // console.log(`random yahooFinance2/historical quotes length: ${Object.keys(result2.quotes).length}`);

    const buildChartResultArray = (val: any) => {
      return null;
    };

    try {
      console.log(`YahooFinance2 / historical - symbol: ${symbol}, period1: ${period1}, period2: ${period2}`);
      const val = await yahooFinance.chart(symbol, { period1, period2 });
      // const val = await prom;
      // console.log(`val keys for ${symbol}: ${JSON.stringify(Object.keys(val))}`)
      // console.log(`val quotes length: ${JSON.stringify(Object.keys(val.quotes).length)}`)
      return val;
      /*
              return prom.then((quotes) => {
                console.log(`historical result for ${symbol}: ${JSON.stringify(quotes)}`);
                return quotes;
              })
              .catch((err) => console.error("Rejected:", err));
      */
    } catch (err) {
      console.error(`yahooFinance.chart Error for symbol: ${symbol}`, err);
      if (err.message.search(/No data found, symbol may be delisted/) > -1) {
        console.error('It appears the symbol no longer exists - ignoring');
      } else {
        throw (err);
      }
    }
    ;
    // return Promise.resolve(null)
  }


  /**
   *
   * @param symbols - one or more symbols
   * @param from - date from (inclusive)
   * @param to - date to (inclusive)
   * @param period - resolution of data - hourly, daily, weekly or monthly - OPTIONAL and defaults to daily
   * @return map of ChartResultArray data consisting of meta and quotes fields.
   */
  async historical(
    symbols: string[],
    from: string,
    to: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    period: Period = Period.d,
  ): Promise<MapChartResultsArray> {
    // DEBUG
    // const query = 'amp.ax';
    // const queryOptions = { period1: '2021-05-08' /* ... */ };
    // const result2 = await yahooFinance.chart(query, queryOptions);
    // console.log(`random yahooFinance2/historicalBatch keys: ${JSON.stringify(Object.keys(result2))}`);
    // console.log(`random yahooFinance2/historicalBatch quotes length: ${Object.keys(result2.quotes).length}`);

    if (!Array.isArray(symbols)) {
      symbols = [symbols];
    }
    const batch = new Map<string, ChartResultArray>();
    try {
      const historicalPromises = symbols.map(s => {
        // console.log(`call Historical(${s}, ${from}, ${to}, ${period})`);
        const promise = this.historicalForSymbol(s, from, to, period);
        // console.log(`  promise returned from call Historical: `, promise);
        return promise;
      });
      await Promise.all(historicalPromises).then((histories: ChartResultArray[]) => {
        histories.forEach(history => {
          if (history && Object.keys(history).length > 0) {
            batch.set(history['meta'].symbol.toUpperCase(), history);
          } else {
            console.error(`history hasn't metadata - its keys are: ${history ? JSON.stringify(Object.keys(history)) : 'history is null'}`);
          }
        });
      });
      return Promise.resolve(batch);
    } catch (error) {
      console.error(`YahooFinance2 / historicalBatch error: `, error);
      return Promise.reject(error);
    }

    // ----
    // for (const s in symbols) {  //.forEach(s => {
    //   this.historical(s, from, to, period)
    //     .then(quote => {
    //       if (quote != null) {
    //         console.log(`yahooFinance2/historicalBatch quote: `, quote)
    //         console.log(`yahooFinance2/historicalBatch keys for ${s}: ${JSON.stringify(Object.keys(quote))}`);
    //         console.log(`yahooFinance2/historicalBatch quotes length: ${Object.keys(quote.quotes).length}`);
    //         batch.set(s, quote);
    //       } else {
    //         console.error(`NO DATA RETURNED - may be because symbol no longer exists`)
    //       }
    //   });
    // };
    // return Promise.resolve(batch);
  }

  quote(
    symbol: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    modules: QuoteModules[] = [QuoteModules.price],
  ): Promise<YahooQuote> {
    /*try {
      const quoteResult = (await quote({ symbol, modules })) as YahooQuote;
      // console.log(`quote Result: ${JSON.stringify(quoteResult, null, 2)}`);
      return resolve(quoteResult);
    } catch (e) {
      console.log(`Quote error: ${JSON.stringify(e)}`);
      return reject(e);
    }*/
    throw ('YahooFinance.quote not impl');
  }

  quoteBatch(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    symbols: string[],
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    modules?: QuoteModules[],
  ): Promise<YahooQuoteBatch> {
    /*try {
      const quotes = await quote({ symbols, modules });
      console.log(`quote batch Result: ${JSON.stringify(quotes, null, 2)}`);
      const batch: ServiceQuoteBatch = new Map<string, YahooQuote>();
      for (const symbol of Object.keys(quotes)) {
        batch.set(symbol, quotes[symbol]);
      }
      return resolve(batch);
    } catch (e) {
      console.log(`Quote batch error: ${e}`);*/
    throw ('YahooFinance.quoteBatch not impl');
  }
}
