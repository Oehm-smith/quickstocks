/**
 * The Yahoo model is exactly the same as the common one used in the application
 * (actually it is the other way around in reality)
 */
import { ServiceHistorical, ServiceHistoricalBatch, ServiceHistoryDataPoint } from '../ServiceProviderHistory.model';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';

export type DataPoint = ServiceHistoryDataPoint;

export type Historical = ServiceHistorical; // DataPoint[];

export type HistoricalBatch = ServiceHistoricalBatch; // Map<string, Historical>;

// YahooFinance2 models
export interface ObjectChartResultsArray {
  [symbol: string]: ChartResultArray;
}

export type MapChartResultsArray = Map<string, ChartResultArray>;

// they do this complicated definition so I will just create my own
export interface ChartResult {
  date: Date; // eg. Nov 07 2013 00:00:00 GMT-0500 (EST),
  open: number; // eg. 45.1,
  high: number; // eg. 50.09,
  low: number; // eg. 44,
  close: number; // eg. 44.9,
  volume: number; // eg. 117701700,
  adjclose?: number; // eg. 44.9,  <-- This lowercase is only difference to local definitions
  symbol?: string; // 'TWTR'
}

export enum SortOrder {
  ascending,
  descending
}