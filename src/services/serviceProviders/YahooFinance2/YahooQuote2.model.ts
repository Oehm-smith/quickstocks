import {
  ServiceQuote,
  ServiceQuoteBatch,
  ServiceQuotePrice,
  ServiceQuoteSummaryDetail,
} from '../ServiceProviderQuote.model';

export type YahooQuoteSummaryDetail = ServiceQuoteSummaryDetail;
export type YahooQuotePrice = ServiceQuotePrice;
export type YahooQuote = ServiceQuote;
export type YahooQuoteBatch = ServiceQuoteBatch;
