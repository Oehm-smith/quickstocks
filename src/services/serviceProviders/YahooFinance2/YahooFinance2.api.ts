import { Period } from '../serviceProvider.API';
import { QuoteModules, ServiceQuote, ServiceQuoteBatch } from '../ServiceProviderQuote.model';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';

export interface YahooFinance2Api {
  /**
   * Lookup historical share value for single symbol.  History means it has to have happened at least the day before.
   * @param symbol - symbol to lookup
   * @param from - date to start at
   * @param to - date (inclusive) to end at
   * @param period (Optional - default is 'd') - 'd'ay, 'w'eek, 'm'onth or 'v' for dividends only
   * @return promise of array of dataPoints for symbol
   *
   * If startDate is undefined then start from first possible historical data
   * If endDate is undefined then finish with most recent data
   * Period provides the freqency of dataPoints within the date range
   */
  historicalForSymbol(
    symbol: string,
    from: string,
    to: string,
    period?: Period,
  ): Promise<ChartResultArray>;

  /**
   * Lookup historical share values for multiple symbols.  History means it has to have happened at least the day before.
   * @param symbols - symbols (array) to lookup
   * @param from - date to start at
   * @param to - date (inclusive) to end at
   * @param period (Optional - default is 'd') - 'd'ay, 'w'eek, 'm'onth or 'v' for dividends only
   * @return Map keyed by symbols with value being promise of array of dataPoints for symbol
   *
   * If startDate is undefined then start from first possible historical data
   * If endDate is undefined then finish with most recent data
   * Period provides the freqency of dataPoints within the date range
   */
  historical(
    symbols: string[],
    from: string,
    to: string,
    period?: Period,
  ): Promise<Map<string, ChartResultArray>>;

  /**
   * Return the current (realtime-ish) data described by the given module for the given symbol.
   * See https://github.com/pilwon/node-yahoo-finance/blob/HEAD/docs/quote.md.
   *
   * @param symbol to get data for
   * @param modules what data to get - defaults to 'price' in implementation
   */
  quote(symbol: string, modules?: QuoteModules[]): Promise<ServiceQuote>;

  /**
   * Return the current (realtime-ish) data described by the given module for the given symbols.
   * See https://github.com/pilwon/node-yahoo-finance/blob/HEAD/docs/quote.md.
   *
   * @param symbols
   * @param modules what data to get - defaults to 'price' in implementation
   */
  quoteBatch(
    symbols: string[],
    modules?: QuoteModules[],
  ): Promise<ServiceQuoteBatch>;
}
