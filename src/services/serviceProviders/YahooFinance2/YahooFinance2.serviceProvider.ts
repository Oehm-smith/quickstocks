import { Period, ServiceProviderAPI } from '../serviceProvider.API';
import moment from 'moment';
import { Injectable } from '@nestjs/common';
import { ServiceHistorical, ServiceHistoricalBatch, ServiceHistoryDataPoint } from '../ServiceProviderHistory.model';
import { QuoteModules, ServiceQuote, ServiceQuoteBatch } from '../ServiceProviderQuote.model';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import { YahooFinance2 } from './YahooFinance2';
import { YahooFinance2Utils } from './YahooFinance2.utils';

const test: ServiceHistoryDataPoint = {
  date: new Date(), // eg. Nov 07 2013 00:00:00 GMT-0500 (EST),
  open: 45.1,
  high: 50.09,
  low: 44,
  close: 44.9,
  volume: 117701700,
  adjClose: 44.9,
  symbol: 'TWTR',
};

const testArr = new Array(test) as ServiceHistorical;


/**
 * This maps the results from querying YahooFinance (YahooFinance.model) to the SpritzerShares.model
 */
@Injectable()
export class YahooFinance2ServiceProvider implements ServiceProviderAPI {
  constructor(private yahooFinance: YahooFinance2) {
  }

  /**
   *
   * @param symbols
   * @param from
   * @param to
   * @param period
   * @return ServiceHistoricalBatch even when only one symbol
   */
  async history(
    symbols: string | string[],
    from: string,
    to: string,
    period: Period = Period.d,
    // ): Promise<ChartResultArray> {
  ): Promise<ServiceHistoricalBatch> {

    // Normalise any given date back to a true date
    const mFFrom: string = from
      ? moment(from).format('YYYY-MM-DD')
      : moment().subtract(7, 'days').format('YYYY-MM-DD');
    const mFTo: string = to ? moment(to).format('YYYY-MM-DD') : undefined;
    // let result: Promise<Historical | HistoricalBatch>;
    // let result: Promise<ChartResultArray>;
    // let standardFormat: Promise<ServiceHistorical | ServiceHistoricalBatch>;
    console.log(`symbols: ${JSON.stringify(symbols)}`);

    function buildHistoricalBatch(results: Map<string, ChartResultArray>): ServiceHistoricalBatch {
      const out = new Map<string, ServiceHistorical>();
      const symbolKeys = results.keys();
      for (const s of results.keys()) {
        out.set(s.toUpperCase(), YahooFinance2Utils.convertChartResulToServiceHistorical(results.get(s)));
      }
      return out;
    }

    // DEBUG
    // const query = 'amp.ax';
    // const queryOptions = { period1: '2021-05-08' /* ... */ };
    // const result2 = await yahooFinance.chart(query, queryOptions);
    // console.log(`random serviceProvider/history keys: ${JSON.stringify(Object.keys(result2))}`);
    // console.log(`random serviceProvider/history quotes length: ${Object.keys(result2.quotes).length}`);


    if (Array.isArray(symbols)) {
      if (symbols.length === 0 || symbols[0] === '') {
        throw (new Error(`Missing symbol`));
      }
    } else {
      if (!symbols || symbols === '') {
        throw (new Error(`Missing symbol`));
      }
      symbols = [symbols];
    }
    return this.yahooFinance.historical(symbols, mFFrom, mFTo, period)
      .then(results => {
        if (results) {
          console.log(`YahooFinance2ServiceProvider / history - results keys: `, [...results.keys()]);
          return buildHistoricalBatch(results);
        } else {
          console.error(`results are null`);
        }
      });
    // .catch(e => {
    //   console.error(`history error -`, e);
    // })
    // return results.

    /*else {
      if (symbols === '') {
        throw (new Error(`Missing symbol`));
      }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const result = this.yahooFinance.historical(symbols, mFFrom, mFTo, period);

      return result.then(r => {
        return YahooFinance2Utils.convertChartResulToServiceHistorical(r);
        // return buildHistoricalBatch(result);
      })
      // .catch(e => {
      //   console.error(`history error -`, e);
      // })
    }*/
    // result.then(data => {
    //   console.log(`result from YF2: ${data}`);
    //   console.log(`${JSON.stringify(data, null, 2)}`);
    // });
    // return result;
    // standardFormat.then(data => {
    //   console.log(`standardFormat: ${data}`)
    //   console.log(`standardFormat: ${JSON.stringify(data, null, 2)}`)
    // })
    // return standardFormat;
  }

  quote(
    symbols: string | string[],
    modules: QuoteModules[],
  ): Promise<ServiceQuote | ServiceQuoteBatch> {
    if (Array.isArray(symbols)) {
      if (symbols.length === 0 || symbols[0] === '') {
        throw (new Error(`Missing symbol`));
      }
      return this.yahooFinance.quoteBatch(symbols, modules);
    } else {
      if (symbols === '') {
        throw (new Error(`Missing symbol`));
      }

      return this.yahooFinance.quote(symbols, modules);
    }
  }
}
