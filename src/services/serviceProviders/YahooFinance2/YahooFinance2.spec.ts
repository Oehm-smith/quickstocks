import { YahooFinance2 } from './YahooFinance2';
import { QuoteModules, ServiceQuote } from '../ServiceProviderQuote.model';
import { TestHelper } from '../../../../test/testData/testHelper';
import { beforeEach, describe, expect, it, xdescribe, xit } from '@jest/globals';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

describe(`YahooFinance2 direct connection with cloud service`, () => {
  let yahooFinance;

  beforeEach(() => {
    yahooFinance = new YahooFinance2();
  });

  describe('historical', () => {
    describe(`one symbol`, () => {
      it(`Get CBA 11.5.20 (2 days - single day actually impossible)`, async () => {
        try {
          // impossible as yahoo-finance2 doesn't allow.  Live with it
          const result = await yahooFinance.historical(
            'cba.ax',
            '2020-05-11',
            '2020-05-12',
          );
          expect(result).toBeDefined();
          const symbols = [...result.keys()];
          expect(symbols.length).toEqual(1);
          expect(symbols).toEqual(['CBA.AX']);
          const cba = result.get('CBA.AX');
          expect(cba.meta).toBeDefined();
          expect(cba.quotes).toBeDefined();
          expect(cba.quotes).toHaveLength(2);
          expect(cba.quotes[0].date.getFullYear()).toEqual(2020);
          expect(cba.quotes[0].date.getMonth()).toEqual(4);
          expect(cba.quotes[0].date.getDate()).toEqual(11);
          expect(cba.quotes[1].date.getFullYear()).toEqual(2020);
          expect(cba.quotes[1].date.getMonth()).toEqual(4);
          expect(cba.quotes[1].date.getDate()).toEqual(12);
        } catch (err) {
          console.log('ERRROR: ', err); // Only shows message
          throw err;
        }
      });

      xit(`Get CBA 11.5.20 - 12.5.20 (2 days - nope it done above now cant do single days)`, async () => {
        // try {

        const result = await yahooFinance.historical(
          'cba.ax',
          '2020-05-11',
          '2020-05-12',
        );
        expect(result).toBeDefined();
        expect(result.meta).toBeDefined();
        expect(result.quotes).toBeDefined();
        expect(result.quotes).toHaveLength(2);
        expect(result.quotes[1].date.getFullYear()).toEqual(2020);
        expect(result.quotes[1].date.getMonth()).toEqual(4);
        expect(result.quotes[1].date.getDate()).toEqual(12);

        expect(result.quotes[0].date.getFullYear()).toEqual(2020);
        expect(result.quotes[0].date.getMonth()).toEqual(4);
        expect(result.quotes[0].date.getDate()).toEqual(11);

      });
    });

    describe(`multiple symbols`, () => {
      it(`Get CBA, BHP 11.5.20 (single day)`, async () => {
        const result = await yahooFinance.historical(
          ['cba.ax', 'bhp.ax'],
          '2020-05-11',
          '2020-05-12',
        );
        expect(result).toBeDefined();

        const symbols = [...result.keys()];
        expect(symbols.length).toEqual(2);
        expect(symbols).toEqual(['CBA.AX', 'BHP.AX']);
        expect(result.get('CBA.AX')).toBeDefined();
        expect(result.get('CBA.AX').meta).toBeDefined();
        expect(result.get('CBA.AX').quotes).toBeDefined();
        expect(result.get('BHP.AX')).toBeDefined();
        expect(result.get('BHP.AX').meta).toBeDefined();
        expect(result.get('BHP.AX').quotes).toBeDefined();
        expect(result.get('CBA.AX').quotes).toHaveLength(2);
        expect(result.get('CBA.AX').quotes[0].date.getFullYear()).toEqual(2020);
        expect(result.get('CBA.AX').quotes[0].date.getMonth()).toEqual(4);
        expect(result.get('CBA.AX').quotes[0].date.getDate()).toEqual(11);
      });

      // Did 2 day test above - because results are inclusive and YahooFinance2 API does not allow for single days
      xit(`Get CBA 11.5.20 - 12.5.20 (2 days )`, async () => {
        try {
          const result = await yahooFinance.historicalBatch(
            ['cba.ax', 'bhp.ax'],
            '2020-05-11',
            '2020-05-13',
          );
          expect(result).toBeDefined();
          expect(result.keys().length).toEqual(2); // cba and bhp
          expect(result.get('CBA.AX')).toBeDefined();
          expect(result.get('CBA.AX').meta).toBeDefined();
          expect(result.get('CBA.AX').quotes).toBeDefined();
          expect(result.get('BHP.AX')).toBeDefined();
          expect(result.get('BHP.AX').meta).toBeDefined();
          expect(result.get('BHP.AX').quotes).toBeDefined();
          expect(result.get('CBA.AX').quotes).toHaveLength(2);
          expect(result.get('CBA.AX').quotes[1].date.getFullYear()).toEqual(2020);
          expect(result.get('CBA.AX').quotes[1].date.getMonth()).toEqual(4);
          expect(result.get('CBA.AX').quotes[1].date.getDate()).toEqual(11);

          expect(result.get('CBA.AX').quotes[0].date.getFullYear()).toEqual(2020);
          expect(result.get('CBA.AX').quotes[0].date.getMonth()).toEqual(4);
          expect(result.get('CBA.AX').quotes[0].date.getDate()).toEqual(12);
        } catch (e) {
          console.error('Error: ', e);
        }

      });

      it(`Get AXJO, CBA, BHP 11.5.20 - 12.5.20 (2 days )`, async () => {
        const result = await yahooFinance.historical(
          ['^axjo', 'cba.ax', 'bhp.ax'],
          '2020-05-11',
          '2020-05-13',
        );
        expect(result).toBeDefined();
        expect(result.size).toEqual(3); // cba and bhp
      }, 20000);
    });

    // No longer using quote (atm)
    xdescribe('quote', () => {
      it(`Get CBA current price`, async () => {
        const result = (await yahooFinance.quote('cba.ax', [
          QuoteModules.price,
        ])) as ServiceQuote;
        expect(result).toBeDefined();
        const price = result.price;
        TestHelper.checkPrice(price, 'cba.ax');
      });

      it(`Get CBA, BHP current price`, async () => {
        const result = await yahooFinance.quoteBatch(
          ['cba.ax', 'bhp.ax'],
          [QuoteModules.price],
        );
        expect(result).toBeDefined();
        const cba = result.get('cba.ax');
        expect(cba).toBeDefined();
        let price = cba.price;
        TestHelper.checkPrice(price, 'cba.ax');

        const bhp = result.get('bhp.ax');
        expect(bhp).toBeDefined();
        price = bhp.price;
        TestHelper.checkPrice(price, 'bhp.ax');
      });
    });
  });
});
