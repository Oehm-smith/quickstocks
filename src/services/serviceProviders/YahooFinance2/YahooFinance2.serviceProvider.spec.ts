import * as colors from 'colors';
import * as sinon from 'sinon';
import { TestHelper } from '../../../../test/testData/testHelper';
import { YahooFinance2 } from './YahooFinance2';
import { YahooFinance2ServiceProvider } from './YahooFinance2.serviceProvider';
import { HistoricalBatch } from './YahooHistory2.model';
import { Test } from '@nestjs/testing';
import { QuoteModules, ServiceQuote } from '../ServiceProviderQuote.model';
import { afterEach, beforeEach, describe, expect, it, xdescribe } from '@jest/globals';
import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import { ServiceHistoricalBatch } from '../ServiceProviderHistory.model';

global.console = require('console'); // to turn off verbose intellij / webstore console.log

interface SuccinctDate {
  year: number;
  month: number;
  day: number;
}

const compareDates = (a: SuccinctDate, b: SuccinctDate): number => {
  if (a.year !== b.year) {
    return b.year - a.year;
  }
  if (a.month !== b.month) {
    return b.month - a.month;
  }
  return b.day - a.day;
};

const checkSymbolDateMap = (
  result: HistoricalBatch,
  symbols: string[],
  expectdDates: SuccinctDate[],
) => {
  //'cba.ax', [[2020, 4, 11]]);
  console.log(
    colors.red(
      `  checkSymbolDateMap - symbols: ${JSON.stringify(symbols)}, dates: ${JSON.stringify(expectdDates)}`,
    ),
  );
  // dates from data are returned descending - make sure the test data is this way also
  expectdDates = expectdDates.sort(compareDates);

  const symbolsInData = [...result.keys()];
  symbols.forEach((symbol) => {
    symbol = symbol.toUpperCase();
    console.debug(`Expect symbol to be defined: ${symbol} - in data they are: ${JSON.stringify(symbolsInData)}`);
    expect(result.get(symbol)).toBeDefined();
    expect(Array.isArray(result.get(symbol))).toBeTruthy();
    expect(result.get(symbol)).toHaveLength(expectdDates.length);
    expectdDates.forEach((expectedDate, i) => {
      expect(result.get(symbol)[i].date instanceof Date).toBeTruthy();
      expect(result.get(symbol)[i].date.getFullYear()).toEqual(
        expectedDate.year,
      );
      expect(result.get(symbol)[i].date.getMonth()).toEqual(
        expectedDate.month,
      );
      expect(result.get(symbol)[i].date.getDate()).toEqual(
        expectedDate.day,
      );
      expect(result.get(symbol)[i].symbol).toEqual(symbol);
    });
  });
};

describe('YahooFinanceServiceProvider', () => {
  describe('historical', () => {
    let yahooFinanceServiceProvider;
    let yahooFinanceStub;
    let yahooFinance;
    describe(`lookup single`, () => {
      yahooFinance = new YahooFinance2();
      beforeEach(async () => {
        yahooFinanceStub = sinon
          .stub(yahooFinance, 'historical')
          .callsFake(
            (
              symbol: string,
              startDate: string,
              endDate: string,
            ): Promise<ChartResultArray> => {
              const file =
                './test/testData/YahooFinance/cba_2020-05-11_to_2020-05-12_ChartResultArray.json';
              return TestHelper.getAndPrepareTestData(
                file,
                startDate,
                endDate,
              ) as Promise<ChartResultArray>;
            },
          );
        yahooFinanceServiceProvider = new YahooFinance2ServiceProvider(
          yahooFinance,
        );
      });

      afterEach(async () => {
        yahooFinanceStub.restore();
      });

      it(`search for CBA Tue 11.5.20`, async () => {
        try {
          const from = '2020-05-11';
          const to = '2020-05-11';
          const range = 1;
          const result = (await yahooFinanceServiceProvider.history(
            'cba.ax',
            from,
            to,
          )) as ServiceHistoricalBatch;
          expect(result).toBeDefined();
          expect(result instanceof Map).toBeTruthy();
          checkSymbolDateMap(
            result,
            ['cba.ax'],
            [{ year: 2020, month: 4, day: 11 }],
          );
        } catch (err) {
          console.log('ERRROR: ', err); // Only shows message
          throw err;
        }

      });

      it(`search for CBA Wed 2020-05-11 and next day`, async () => {
        try {
          const from = '2020-05-11';
          const to = '2020-05-12';
          const result = (await yahooFinanceServiceProvider.history(
            'cba.ax',
            from,
            to,
          )) as ServiceHistoricalBatch;
          expect(result instanceof Map).toBeTruthy();
          checkSymbolDateMap(
            result,
            ['cba.ax'],
            [{ year: 2020, month: 4, day: 11 }, { year: 2020, month: 4, day: 12 }],
          );
        } catch (err) {
          console.log('ERRROR: ', err); // Only shows message
          throw err;
        }
      });

      it(`should fail if no symbol`, async () => {
        const from = '2020-05-13';
        return expect(
          yahooFinanceServiceProvider.history('', from, from),
        ).rejects.toThrowError(`Missing symbol`);
      });
    });

    describe(`lookup batch`, () => {
      yahooFinance = new YahooFinance2();
      beforeEach(async () => {
        yahooFinanceStub = sinon
          .stub(yahooFinance, 'historical')
          .callsFake(
            (
              symbol: string,
              startDate: string,
              endDate: string,
            ): Promise<Map<string, ChartResultArray>> => {
              const file =
                './test/testData/YahooFinance/cba_bhp_2020-05-11_to_2020-05-12_MapChartResultsArray.json';
              return TestHelper.getAndPrepareTestData(
                file,
                startDate,
                endDate,
              ) as Promise<Map<string, ChartResultArray>>;
            },
          );
        yahooFinanceServiceProvider = new YahooFinance2ServiceProvider(
          yahooFinance,
        );
      });

      afterEach(async () => {
        yahooFinanceStub.restore();
      });

      it(`search for CBA, BHP Tue 11.5.20`, async () => {
        // try {
        const from = '2020-05-11';
        const to = '2020-05-11';
        const result = (await yahooFinanceServiceProvider.history(
          ['cba.ax', 'bhp.ax'],
          from,
          to,
        )) as HistoricalBatch;
        expect(result).toBeDefined();
        expect(result instanceof Map).toBeTruthy();

        checkSymbolDateMap(
          result,
          ['cba.ax', 'bhp.ax'],
          [{ year: 2020, month: 4, day: 11 }],
        );
        // } catch (err) {
        //   console.log('ERRROR: ', err); // Only shows message
        // }

      });

      it(`search for CBA, BHP Wed 2020-05-11 and next day`, async () => {
        // try {
        const from = '2020-05-11';
        const to = '2020-05-12';
        const result = (await yahooFinanceServiceProvider.history(
          ['cba.ax', 'bhp.ax'],
          from,
          to,
        )) as HistoricalBatch;
        expect(result).toBeDefined();
        expect(result instanceof Map).toBeTruthy();

        checkSymbolDateMap(
          result,
          ['CBA.AX', 'BHP.AX'],
          [
            { year: 2020, month: 4, day: 12 },
            {
              year: 2020,
              month: 4,
              day: 11,
            },
          ],
        );
        // }
        // (error) {
        //   console.error("ERROR: ", error)
        // }

      });

      it(`should fail if no symbol`, async () => {
        const from = '2020-05-13';
        return expect(
          yahooFinanceServiceProvider.history([''], from, from),
        ).rejects.toThrowError(`Missing symbol`);
      });
    });
  });
  // No longer (currently) using quote
  xdescribe(`quote`, () => {
    let yahooFinanceServiceProvider;

    // I'm not going to wet my pants over trying to abstract the network away from this test like I did with 'historical' above
    // Since the YahooFinance2ServiceProvider pretty much just passes through the result from YahooFinance which is from Finance.Yahoo
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [YahooFinance2ServiceProvider, YahooFinance2],
      }).compile();

      yahooFinanceServiceProvider = moduleRef.get<YahooFinance2ServiceProvider>(
        YahooFinance2ServiceProvider,
      );
    });
    describe(`single symbol`, () => {
      it(`Get CBA current price`, async () => {
        const result = (await yahooFinanceServiceProvider.quote('cba.ax', [
          QuoteModules.price,
        ])) as ServiceQuote;
        expect(result).toBeDefined();
        const price = result.price;
        TestHelper.checkPrice(price, 'cba.ax');
      });
    });

    describe(`batch symbols`, () => {
      it(`Get CBA, BHP current price`, async () => {
        const result = await yahooFinanceServiceProvider.quote(
          ['cba.ax', 'bhp.ax'],
          [QuoteModules.price],
        );
        expect(result).toBeDefined();
        const cba = result.get('cba.ax');
        expect(cba).toBeDefined();
        let price = cba.price;
        TestHelper.checkPrice(price, 'cba.ax');

        const bhp = result.get('bhp.ax');
        expect(bhp).toBeDefined();
        price = bhp.price;
        TestHelper.checkPrice(price, 'bhp.ax');
      });
    });
  });
});
