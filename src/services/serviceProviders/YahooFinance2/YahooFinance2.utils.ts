import { ChartResultArray } from 'yahoo-finance2/dist/esm/src/modules/chart';
import {
  ServiceHistorical,
  ServiceHistoricalBatch,
  ServiceHistoricalBatchByDate,
  ServiceHistoryDataPoint,
} from '../ServiceProviderHistory.model';
import { DateUtils } from '../../../utils/DateUtils';
import { SortOrder } from './YahooHistory2.model';

export class YahooFinance2Utils {
  static convertChartResulToServiceHistorical = (result: ChartResultArray): ServiceHistorical => {
    // console.log(`convertChartResulToServiceHistorical -`)
    // return result.then(c => {
    // console.log(`  in charResultArray`)
    // console.log(JSON.stringify(c, null, 2))
    if (result && result.meta && result.quotes) {
      const out = result['quotes'].map(q => {
        const x = {
          ...q,
          symbol: result['meta'].symbol,
          adjClose: q.adjclose,
          date: new Date(q.date),
        } as ServiceHistoryDataPoint;
        return x;
      });
      // console.log(`  out ServiceHistorical`);
      // console.log(JSON.stringify(out, null, 2));
      return out;
    } else {
      // const symbols = [...(result as MapChartResultsArray).keys()];
      // if (symbols.length)
      //   const firstSymbol = [0]
      return [];
    }
    // })
  };

  /*static convertChartResulToServiceHistoricalMOD = (result: ChartResultArray | MapChartResultsArray): ServiceHistorical | ServiceHistoricalBatch => {
    // console.log(`convertChartResulToServiceHistorical -`)
    // return result.then(c => {
    // console.log(`  in charResultArray`)
    // console.log(JSON.stringify(c, null, 2))
    const doConvert = (cra: ChartResultArray): ServiceHistorical => {
      return cra['quotes'].map(q => {
        const x = { ...q, symbol: result['meta'].symbol, adjClose: q.adjclose } as ServiceHistoryDataPoint;
        return x;
      });
    }
    if (result && result.meta && result.quotes) {
      // const out = result['quotes'].map(q => {
      //   const x = { ...q, symbol: result['meta'].symbol, adjClose: q.adjclose } as ServiceHistoryDataPoint;
      //   return x;
      // });
      const out = doConvert(result as ChartResultArray);
      console.log(`  out ServiceHistorical`);
      console.log(JSON.stringify(out, null, 2));
      return out;
    } else {
      const resultAsMap = (result as MapChartResultsArray);
      const symbols = [...resultAsMap.keys()];
      if (symbols.length) {
        const first = resultAsMap.get(symbols[0]);
        if (first['meta']) {
          const shB = new Map<string, ServiceHistorical>();
          for (const s of symbols) {
            const out = doConvert(resultAsMap.get(s))
            shB.set(s, out)
          }
          return shB as ServiceHistoricalBatch
        } else {
          throw Error(`Expecting a 'meta' on result as MapChartResultsArray but none - it is: ${JSON.stringify(first)}`)
        }
      }
      return [];
    }
  }*/

  static getUniqueSymbols = (data: ServiceHistorical | ServiceHistoricalBatch): string[] => {
    const getUniqueArray = (inx: string[]): string[] => {
      const aSet = new Set(inx);
      return [...aSet.values()];
    };
    if (data instanceof Map) {
      return getUniqueArray([...data.keys()]);
    }
    return getUniqueArray((data as ServiceHistorical).map(d => d.symbol));
  };

  /**
   * Let's always work with ServiceHistoricalBatch.
   * @param data
   */
  static createServiceHistoryMap = (data: ServiceHistorical | ServiceHistoricalBatch): ServiceHistoricalBatch => {
    if (data instanceof Map) {
      return data as ServiceHistoricalBatch;
    }
    // const symbols = (data as ServiceHistorical).map(d => d.symbol)
    const symbols = YahooFinance2Utils.getUniqueSymbols(data);
    if (symbols.length > 1) {
      throw Error(`ServiceHistorical data contains more than one symbol: ${JSON.stringify(data, null, 2)}`);
    }
    const serviceHistoricalBatch = new Map<string, ServiceHistorical>();
    serviceHistoricalBatch.set(symbols[0].toUpperCase(), data);
    return serviceHistoricalBatch;
  };

  static convertServiceHistoricalToBatch = (data: ServiceHistorical | ServiceHistoricalBatch): ServiceHistoricalBatch => {
    if (data instanceof Map) {
      return data as ServiceHistoricalBatch;
    }
    const symbols = [...new Set(data.map(d => d.symbol)).values()];
    const numberOfDates = data.length;
    console.log(`DataHelpers / convertServiceHistoricalToSymbolMap - symbols: ${symbols}`);
    const serviceHistoricalMap = new Map<string, ServiceHistorical>();
    for (const s of symbols) {
      serviceHistoricalMap.set(s.toUpperCase(), data);
    }
    return serviceHistoricalMap;
  };

  /**
   * Translate Map<string, ServiceHistorical> == Map<string, ServiceHistoryDataPoint[]> to ServiceHistorical[] where the
   * inner array are the symbols in order for the same date and the other array are all the dates in ascending order
   * (which should be the natural order in the data).
   * @param symbolsData
   * @return ServiceHistorical[] == ServiceHistoryDataPoint[][]
   */
  static aggregateByDate = (symbolsData: ServiceHistoricalBatch, order: SortOrder): ServiceHistoricalBatchByDate => {
    // assumption - all symbols contain the same number of data points
    // assumption - all data points for all the symbols have the same date (and are in date-order)
    // However I will be checking this
    const symbols = YahooFinance2Utils.getUniqueSymbols(symbolsData);

    const orderFunction = (order: SortOrder) => {
      const compareDates = (a: Date, b: Date): number => {
        if (a.getFullYear() !== b.getFullYear()) {
          return b.getFullYear() - a.getFullYear();
        }
        if (a.getMonth() !== b.getMonth()) {
          return b.getMonth() - a.getMonth();
        }
        return b.getDate() - a.getDate();
      };

      return function(p1: Date, p2: Date) {
        switch (order) {
          case SortOrder.ascending:
            return compareDates(p2, p1);
            break;
          case SortOrder.descending:
          default:
            return compareDates(p1, p2);
        }
      };
    };

    const dates = YahooFinance2Utils.getDates(symbolsData).sort(orderFunction(order));
    console.log(`Dates: ${JSON.stringify(dates)}`);
    const serviceHistoricalBatchByDate: ServiceHistoricalBatchByDate = new Map<number, ServiceHistorical>();
    const byDate = new Map<number, ServiceHistorical>();
    for (const [index, date] of dates.entries()) {
      const dataForSymbolsAtDate = [] as ServiceHistorical;
      for (const s of symbols) {
        const datePerSymbol = YahooFinance2Utils.getDataForSymbolAtDate(s, date, symbolsData);
        dataForSymbolsAtDate.push(...datePerSymbol);
      }
      byDate.set(date.getTime(), dataForSymbolsAtDate);
    }
    return byDate;
  };

  /**
   * @return the dates for share data and confirm all symbols have that and only those dates.
   *
   * @param symbolsData
   */
  static getDates = (symbolsData: Map<string, ServiceHistorical>, sortOrder: SortOrder = SortOrder.ascending): Date[] => {
    const symbols = YahooFinance2Utils.getUniqueSymbols(symbolsData);
    if (symbols.length == 0) {
      return [];
    }
    const getSymbolDates = index => symbols.length > index ? symbolsData.get(symbols[0]).map(s => s.date) : [];

    const dates0 = getSymbolDates(0);

    // if (!DateUtils.isSorted(dates0, 'desc')) {
    //   throw new Error(`Dates are not sorted: ${JSON.stringify(dates0)}`);
    // }
    for (const [i, s] of symbols.entries()) {
      if (i > 0) {
        const dates_more = getSymbolDates(i);
        if (!DateUtils.areDateListsEqual(dates0, dates_more, sortOrder)) {
          throw new Error(`Dates are not equal between symbols - ${symbols[0]} and ${symbols[i]} - dates are - date0: ${JSON.stringify(dates0)}, date${i}: ${JSON.stringify(dates_more)}`);
        }
      }
    }
    return dates0;
  };
  /**
   * @return the ServiceHistoryDataPoint as array (ServiceHistorical) for date for given symbol from given symbolsData.
   * @param symbol
   * @param date
   * @param symbolsData
   */
  static getDataForSymbolAtDate = (symbol: string, date: Date, symbolsData: Map<string, ServiceHistorical>): ServiceHistorical => {
    if (symbolsData.has(symbol)) {
      const symbolData = symbolsData.get(symbol);
      const dataForDate = symbolData.filter(s => {
        return s.date.getTime() == date.getTime();
      });
      if (!dataForDate.length) {
        console.error(`Have data for symbol: ${symbol}, but there is no date: ${date}.  The dates it has are: ${JSON.stringify(YahooFinance2Utils.getDates(symbolsData))}`);
        return [];
      } else {
        return dataForDate;
      }
    } else {
      console.error(`symbolsData does not have symbol: ${symbol}`);
    }
    return null;
  };
}