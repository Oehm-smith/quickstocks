import { Period, ServiceProviderAPI, ServiceProviderAPI_TOKEN } from './serviceProvider.API';
import { Inject, Injectable } from '@nestjs/common';
import { ServiceHistoricalBatch } from './ServiceProviderHistory.model';
import { QuoteModules, ServiceQuote, ServiceQuoteBatch } from './ServiceProviderQuote.model';

import dotenv from 'dotenv';

dotenv.config();

@Injectable()
/**
 * The ServiceProvider performs the round robin retry action by cycling through the different providers and retrying
 * if necessary.  Until an answer is received.
 */
export class ServiceProvider implements ServiceProviderAPI {
  // TODO should be choosing the <Provider>ServiceProvider through a factory
  // But because I only have YahooFinance at the moment and the common model is the same as  YH F, then I can just
  // pass everything through.
  constructor(
    @Inject(ServiceProviderAPI_TOKEN)
    private readonly serviceProvider: ServiceProviderAPI,
  ) {
  }

  // YahooFinance2ServiceProvider
  history(
    symbols: string,
    from: string,
    to: string,
    period: Period = Period.d,
  ): Promise<ServiceHistoricalBatch> {
    const fixSymbols = (syms) => syms.split(/,/).map(s => s.replace(/[^\w\d^\.]+/g, '').trim());

    const symbolsArray = fixSymbols(symbols);

    return this.serviceProvider.history(symbolsArray, from, to, period);
  }

  quote(
    symbols: string | string[],
    modules: QuoteModules[],
  ): Promise<ServiceQuote | ServiceQuoteBatch> {
    return this.serviceProvider.quote(symbols, modules);
  }
}
