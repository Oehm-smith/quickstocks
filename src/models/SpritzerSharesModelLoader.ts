/**
 * Loader for all intermediate models to the SpritzerSharesModel.
 */
import { SpritzerSharesModel } from './SpritzerSharesModel';
import { AlphaVantageModel } from './AlphaVantageModel';

export class SpritzerSharesModelLoader {
  static load(intemediateFormat: any): SpritzerSharesModel {
    // TODO need to work out this instanceOf so can have alt types
    // if (intemediateFormat instanceof AlphaVantageModel) {
    return SpritzerSharesModelLoader.loadAlphaVantage(intemediateFormat);
    // }
  }

  static loadAlphaVantage(
    intermediateFormat: AlphaVantageModel,
  ): SpritzerSharesModel {
    const intermediateFormatClass: SpritzerSharesModel =
      new SpritzerSharesModel();
    intermediateFormatClass.metaData = intermediateFormat.MetaData;
    if (intermediateFormat.TimeSeriesDaily) {
      Object.keys(intermediateFormat.TimeSeriesDaily).forEach((dayDate) => {
        intermediateFormatClass.addData(
          dayDate,
          intermediateFormat.TimeSeriesDaily[dayDate],
        );
      });
    }
    if (intermediateFormat.TimeSeriesWeekly) {
      Object.keys(intermediateFormat.TimeSeriesWeekly).forEach((weekDate) => {
        intermediateFormatClass.addData(
          weekDate,
          intermediateFormat.TimeSeriesWeekly[weekDate],
        );
      });
    }
    return intermediateFormatClass;
  }
}
