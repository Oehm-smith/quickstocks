import { INTERVAL } from './MomentUnits';
import { Moment } from 'moment';

export interface TimeSeriesData {
  open: string;
  high: string;
  low: string;
  close: string;
  volume: string;
}

/**
 * Normalised version of MetaDataAV
 */
export class MetaData {
  information: string;
  symbol: string;
  lastRefreshed: Moment;
  outputSize: string;
  timeZone: string;
  dataInterval: INTERVAL;

  constructor(Information, Symbol, LastRefreshed, OutputSize, TimeZone) {
    this.information = Information;
    this.symbol = Symbol;
    this.lastRefreshed = LastRefreshed;
    this.outputSize = OutputSize;
    this.timeZone = TimeZone;
  }
}
