import { MetaData, TimeSeriesData } from './ModelsCommonElements';

/**
 * For AlphaVista cloud API - notice the difficult to work with key names
 */
export class MetaDataAV {
  '1. Information': string;
  '2. Symbol': string;
  '3. Last Refreshed': string;
  '4. Output Size': string;
  '5. Time Zone': string;

  constructor(one, two, three, four, five) {
    this['1. Information'] = one;
    this['2. Symbol'] = two;
    this['3. Last Refreshed'] = three;
    this['4. Output Size'] = four;
    this['5. Time Zone'] = five;
  }
}

export interface TimeSeriesAV {
  [date: string]: {
    '1. open': string;
    '2. high': string;
    '3. low': string;
    '4. close': string;
    '5. volume': string;
  };
}

export interface AlphaVantageCloudModel {
  'Meta Data': MetaDataAV;
  'Time Series Daily'?: TimeSeriesAV;
  'Time Series Weekly'?: TimeSeriesAV;
}

/**
 * Normalised version of API Models - notice the slightly nicer to work with keys.  This uses some common elements that are
 * also used by the internal models (and thus why they are distributed in another source file.
 */
export interface TimeSeries {
  [date: string]: TimeSeriesData;
}

/**
 * Normalised version of AlphaVantageAV
 */
export class AlphaVantageModel {
  MetaData: MetaData;
  TimeSeriesDaily?: TimeSeries;
  TimeSeriesWeekly?: TimeSeries;

  constructor(metaData?: MetaData) {
    if (metaData) {
      this.MetaData = metaData;
    }
  }
}
