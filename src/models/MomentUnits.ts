export enum UNITS {
  'year' = 'years1',
  'years' = 'years2',
  'y' = 'years3',
  'month' = 'months1',
  'months' = 'months2',
  'M' = 'months3',
  'week' = 'weeks1',
  'weeks' = 'weeks2',
  'w' = 'weeks3',
  'day' = 'days1',
  'days' = 'days2',
  'd' = 'days3',
  'hour' = 'hours1',
  'hours' = 'hours2',
  'h' = 'hours3',
  'minute' = 'minutes3',
  'minutes' = 'minutes2',
  'm' = 'minutes1',
  'second' = 'seconds1',
  'seconds' = 'seconds2',
  's' = 'seconds3',
  'millisecond' = 'milliseconds1',
  'milliseconds' = 'milliseconds2',
  'ms' = 'milliseconds3',
}

export enum INTERVAL {
  OneMin = 'OneMin',
  FiveMin = 'FiveMin',
  FifteenMin = 'FifteenMin',
  ThirtyMin = 'ThirtyMin',
  SixtyMin = 'SixtyMin',
  Daily = 'Daily',
  Weekly = 'Weekly',
  Monthly = 'Monthly',
}

export class MomentUnit {
  unit: UNITS;
  unitString: string;
  multiplier: number; // of the unit

  constructor(unit, multiplier) {
    // const nbrIndex = us.search(/([^\d]+)/)
    // this.unitString = nbrIndex === -1 ? us : us.substring(0, nbrIndex)
    this.unit = unit;
    this.unitString = unit.toString();
    // [1] substring(0, us.length - 1)  // drop digit at end
    this.multiplier = multiplier;
  }
}

// TODO - I don't like this being in models
// @Injectable()
export class MomentUnits {
  static units: { [unit: string]: MomentUnit } = {};

  static initialize() {
    this.units.years = new MomentUnit('years', 1);
    this.units.months = new MomentUnit('months', 1);
    this.units.weeks = new MomentUnit('weeks', 1);
    this.units.days = new MomentUnit('days', 1);
    this.units.hours = new MomentUnit('hours', 1);
    this.units.minutes1 = new MomentUnit('minutes', 1);
    this.units.minutes5 = new MomentUnit('minutes', 5);
    this.units.minutes15 = new MomentUnit('minutes', 15);
    this.units.minutes30 = new MomentUnit('minutes', 30);
    this.units.minutes60 = new MomentUnit('minutes', 60);
    this.units.seconds = new MomentUnit('seconds', 1);
    this.units.milliseconds = new MomentUnit('milliseconds', 1);
  }

  /**
   * @param interval to return units as used by moment and the multiplier
   * @returns [unit as used in moment add/subtract, number of the unit]
   */
  static intervalForMoment(interval: INTERVAL): MomentUnit {
    switch (interval) {
      case INTERVAL.OneMin:
        return MomentUnits.units.minutes1;
      case INTERVAL.FiveMin:
        return MomentUnits.units.minutes5;
      case INTERVAL.FifteenMin:
        return MomentUnits.units.minutes15;
      case INTERVAL.ThirtyMin:
        return MomentUnits.units.minutes30;
      case INTERVAL.SixtyMin:
        return MomentUnits.units.minutes60;
      case INTERVAL.Daily:
        return MomentUnits.units.days;
      case INTERVAL.Weekly:
        return MomentUnits.units.weeks;
      case INTERVAL.Monthly:
        return MomentUnits.units.months;
      default:
        throw new Error(`intervalForMoment - no such interval: ${interval}`);
    }
  }
}

MomentUnits.initialize();
