/**
 * This is the generic internal model (data structure) used to hold stock information.  It will also be the lowest level
 * model class and will depend on nothing else - things can only depend on it.
 *
 * AlphaVantageModel contains models for getting the data out of AlphaVantage and in to a data format we can work with.
 * Other service provider models will be the same.
 *
 * A smarter version of the AlphaVantageModel.
 * The main difference is the use of Maps for the time series data so can both iterate, treat like and array and retrieve by key.
 */
import { MetaData, TimeSeriesData } from './ModelsCommonElements';

export type TimeSeriesDataMap = Map<string, TimeSeriesData>;

/**
 * Generic no Provider API specific normalised form of Metadata
 */
// export class GenericMetaData {
//   information: string;
//   symbol: string;
//   lastRefreshed: Moment;
//   outputSize: string;   // Maybe dateRange (dateStart, dateEnd) instead of this AlphaVantage type
//   timeZone: string;
//   dataInterval: INTERVAL;
//
//   constructor(Information, Symbol, LastRefreshed, OutputSize, TimeZone) {
//     this.information = Information;
//     this.symbol = Symbol;
//     this.lastRefreshed = LastRefreshed;
//     this.outputSize = OutputSize;
//     this.timeZone = TimeZone;
//   }
// }

export class SpritzerSharesModel {
  metaData: MetaData;
  timeSeriesData?: TimeSeriesDataMap;

  constructor(metaData?: MetaData) {
    this.metaData = metaData;
    this.timeSeriesData = new Map<string, TimeSeriesData>();
  }

  addData(dateTimeString: string, t: TimeSeriesData) {
    this.timeSeriesData.set(dateTimeString, t);
  }

  toString(): string {
    return (
      `[SpritzerSharesModel - metaData: ${JSON.stringify(this.metaData)}\n` +
      `  timeSeriesData: ${this.returnMapString(this.timeSeriesData)}\n` +
      `]`
    );
  }

  private returnMapString(map: TimeSeriesDataMap): string {
    const mapString = [];
    map.forEach((value, key) => {
      mapString.push(`${key} => ${JSON.stringify(value)}`);
    });
    return mapString.join(',\n    ');
  }
}
