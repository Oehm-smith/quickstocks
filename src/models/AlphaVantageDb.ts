// import {Model, MongooseIndex} from '@tsed/mongoose'
// import {Description} from '@tsed/swagger'

import { TimeSeriesData } from './ModelsCommonElements';

/**
 * Use as the DB Model
 */
// @Model({collection: 'AlphaVantageData'})
// @MongooseIndex({symbol: 1, date: 1}, {unique: 1})
export class AlphaVantageDb {
  /* tslint:disable*/
  // @Description('Object id')
  _id?: string;
  /* tslint:enable */

  // @Description(`stock symbol`)
  symbol: string;

  // @Description(`date or date range in UNIX (ms since 1070) format`)
  date: Date;

  // @Description(`data returned in JSON format`)
  data: TimeSeriesData;
}

/**
 * Use to query the DB Model
 */
export class AvDbModelQuery {
  /* tslint:disable*/
  // @Description('Object id')
  _id?: string;
  /* tslint:enable */

  // @Description(`stock symbol`)
  symbol: string;

  // @Description(`date or date range in UNIX (ms since 1070) format`)
  date?: Date;

  // @Description(`for date range queries.  Both from and to should exist`)
  dateFrom?: string;

  // @Description(`for date range queries.  Both from and to should exist`)
  dateTo?: string;

  // @Description(`data returned in JSON format`)
  data?: TimeSeriesData;
}
