import { INTERVAL, MomentUnits } from './MomentUnits';
import { describe, expect, it } from '@jest/globals';

describe('MomentUnits', () => {
  it('INTERVALtoMinutes', () => {
    expect(MomentUnits.intervalForMoment(INTERVAL.OneMin).multiplier).toEqual(
      1,
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.FiveMin).multiplier).toEqual(
      5,
    );
    expect(
      MomentUnits.intervalForMoment(INTERVAL.FifteenMin).multiplier,
    ).toEqual(15);
    expect(
      MomentUnits.intervalForMoment(INTERVAL.ThirtyMin).multiplier,
    ).toEqual(30);
    expect(
      MomentUnits.intervalForMoment(INTERVAL.SixtyMin).multiplier,
    ).toEqual(60);
    expect(MomentUnits.intervalForMoment(INTERVAL.Daily).multiplier).toEqual(
      1,
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.Weekly).multiplier).toEqual(
      1,
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.Monthly).multiplier).toEqual(
      1,
    );

    expect(MomentUnits.intervalForMoment(INTERVAL.OneMin).unit).toEqual(
      'minutes',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.FiveMin).unit).toEqual(
      'minutes',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.FifteenMin).unit).toEqual(
      'minutes',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.ThirtyMin).unit).toEqual(
      'minutes',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.SixtyMin).unit).toEqual(
      'minutes',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.Daily).unit).toEqual('days');
    expect(MomentUnits.intervalForMoment(INTERVAL.Weekly).unit).toEqual(
      'weeks',
    );
    expect(MomentUnits.intervalForMoment(INTERVAL.Monthly).unit).toEqual(
      'months',
    );
  });
});
